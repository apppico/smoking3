//
//  AddSmok.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/12/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(AddSmok)

class AddSmok: NSManagedObject {
    
    @NSManaged var imgSmok:NSData
    @NSManaged var nameSmok:String
    @NSManaged var priceSmok:String
    @NSManaged var typeSmok:String
   
}
