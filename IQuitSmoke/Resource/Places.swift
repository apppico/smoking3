//
//  Placesmoke.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/11/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(Places)

class Places: NSManagedObject {
    
        @NSManaged var image:UIImage?
        @NSManaged var text:String
        @NSManaged var time:String
        @NSManaged var title:String
        @NSManaged var type_:String
    
}
