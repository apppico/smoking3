//
//  Image.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/8/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(Image)

class Image: NSManagedObject {
    
    @NSManaged var imgProfile:NSData   
}
