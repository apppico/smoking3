//
//  Staff_ViewController.swift
//  ไทยไร้ควัน
//
//  Created by Apinat Jitrat on 11/16/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class Staff_ViewController: UIViewController {

    
    @IBOutlet var lb_HeadText: UILabel!
    
    @IBOutlet var lb_Adviser: UILabel!
    @IBOutlet var lb_Adviser1: UILabel!
    @IBOutlet var lb_Adviser2: UILabel!
    
    @IBOutlet var lb_Board: UILabel!
    @IBOutlet var lb_Board1: UILabel!
    @IBOutlet var lb_Board2: UILabel!
    @IBOutlet var lb_Board3: UILabel!
    @IBOutlet var lb_Board4: UILabel!
    @IBOutlet var lb_Board5: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        lb_HeadText.text    = "รายนามคณะผู้จัดทำ"
//        
//        lb_Adviser.text     = "ที่ปรึกษา"
//        lb_Adviser1.text    = "ศ.พญ.สมศรี    เผ่าสวัสดิ์"
//        lb_Adviser2.text    = "ศ.นพ.รณชัย    คงสกนธ์"
//        
//        lb_Board.text   = "คณะกรรมการ"
//        lb_Board1.text  = "ผศ.นพ.สุทัศน์ รุ่งเรืองหิรัญญา         ประธาน"
//        lb_Board2.text  = "รศ.ดร.จินตนา  ยูนิพันธุ์                  กรรมการ"
//        lb_Board3.text  = "คุณแสงเดือน  สุวรรณรัศมี             กรรมการ"
//        lb_Board4.text  = "ภก.คฑา บัณฑิตานุกูล                   กรรมการ"
//        lb_Board5.text  = "พริษฐ์ รัตนกุล เสรีเริงฤทธิ์             กรรมการ"
    
    }
    
    
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
            
        }
    }

}
