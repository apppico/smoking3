//
//  Pages4.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/26/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class Pages4: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lb_Text: UILabel!
    
    var drawerController:KYDrawerController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
            self.lb_Text.text = myInstance.textPages[3]
            self.imageView.image = UIImage(named: "img_remove_device")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
