//
//  Pages3.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/26/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class Pages3: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lb_Text: UILabel!
    @IBAction func actionPages(sender: UIButton) {
        
        connextNavigation()
   
    }

    var drawerController:KYDrawerController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
            self.lb_Text.text = myInstance.textPages[2]
            self.imageView.image = UIImage(named: "pages_3")

    }
    
    func connextNavigation(){
        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainTabViewPages()
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
