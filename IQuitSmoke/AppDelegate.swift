
//
//  AppDelegate.swift
//  IQuitSmoke
//
//  Created by iAmMac on 8/13/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import CoreData
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    
    var window: UIWindow?
    let locationManager = CLLocationManager()

    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        // Override point for customization after application launch.
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Sound | .Alert | .Badge, categories: nil))
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        notication()
       //Navigation ----------
        var navigationBarAppearace = UINavigationBar.appearance()
         navigationBarAppearace.tintColor = UIColor.whiteColor()
        navigationBarAppearace.barTintColor = UIColor(rgba: "#0277BD")
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Avenir", size: 20)!]
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
//        var storyboard: UIStoryboard = self.grabStoryboard()
//        self.window!.rootViewController = storyboard.instantiateInitialViewController() as? UIViewController
//        self.window!.makeKeyAndVisible()
//        self.window!.rootViewController = storyboard.instantiateInitialViewController() as? UIViewController
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func notication() {
        var firstAction:UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        firstAction.identifier = "สูบ"
        firstAction.title = "สูบ"
        
    // ไม่เด้งเข้าแอพ    firstAction.activationMode = UIUserNotificationActivationMode.Background
        firstAction.activationMode = UIUserNotificationActivationMode.Foreground
        firstAction.destructive = true
        firstAction.authenticationRequired = false
        
        var secondAction:UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        secondAction.identifier = "ไม่สูบ"
        secondAction.title = "ไม่สูบ"
        
        secondAction.activationMode = UIUserNotificationActivationMode.Foreground
        secondAction.destructive = false
        secondAction.authenticationRequired = false
    
        // category
        var firstCategory:UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
        firstCategory.identifier = "SMOKING"
        
        if(firstCategory.identifier == "SMOKING"){
            
            let defaultActions:NSArray = [firstAction, secondAction]
            let minimalActions:NSArray = [firstAction, secondAction]
            
            firstCategory.setActions(defaultActions as [AnyObject], forContext: UIUserNotificationActionContext.Default)
            firstCategory.setActions(minimalActions as [AnyObject], forContext: UIUserNotificationActionContext.Minimal)
            
            // NSSet of all our categories
            
            let categories:NSSet = NSSet(objects: firstCategory)
            
            let types:UIUserNotificationType = UIUserNotificationType.Alert | UIUserNotificationType.Badge
            
            let mySettings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: types, categories: categories as Set<NSObject>)
            
            UIApplication.sharedApplication().registerUserNotificationSettings(mySettings)
            
        }else{
            noticationRandom()
        }
       
    }
    
    func noticationRandom() {
        var firstCategory:UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
        firstCategory.identifier = "RANDOM"
        
        // NSSet of all our categories
        
        let categories:NSSet = NSSet(objects: firstCategory)
        
        let types:UIUserNotificationType = UIUserNotificationType.Alert | UIUserNotificationType.Badge
        
        let mySettings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: types, categories: categories as Set<NSObject>)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(mySettings)
    }
    
    func application(application: UIApplication,
        handleActionWithIdentifier identifier:String?,
        forLocalNotification notification:UILocalNotification,
        completionHandler: (() -> Void)){
            
            if (identifier == "สูบ"){
                NSNotificationCenter.defaultCenter().postNotificationName("actionOnePressed", object: nil)
                    connextNavigation()
                
            }else if (identifier == "ไม่สูบ"){
                NSNotificationCenter.defaultCenter().postNotificationName("actionTwoPressed", object: nil)
                    connextNavigation()
            }
            completionHandler()
    }
    
    func connextNavigation(){
        var drawerController:KYDrawerController?

        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainCalendar()
        }else{
            
        }
    }
  
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func grabStoryboard() -> UIStoryboard {
        var screenHeight = UIScreen.mainScreen().bounds.size.height
        var storyboard: UIStoryboard
        
        switch screenHeight {
        case 480:
            storyboard = UIStoryboard(name: "Main-6", bundle: nil)
        case 568:
            storyboard = UIStoryboard(name: "Main-6", bundle: nil)
        case 667:
            storyboard = UIStoryboard(name: "Main-6", bundle: nil)
        case 736:
            storyboard = UIStoryboard(name: "Main-6", bundle: nil)
        default:
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }
        return storyboard
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.rs..SampleCoreData" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Model_data", withExtension: "momd")
        return NSManagedObjectModel(contentsOfURL: modelURL!)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Model_data.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict as [NSObject : AnyObject])
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
    
    // MARK - Core MapKit 
    func handleRegionEvent(region: CLRegion) {
        // Show an alert if application is active
        if UIApplication.sharedApplication().applicationState == .Active {
            if let message = notefromRegionIdentifier(region.identifier) {
                if let viewController = window?.rootViewController {
                    showSimpleAlertWithTitle(nil, message: "คุณเริ่มเข้าใกล้พื้นที่เสี่ยงต่อการสูบบุหรี่", viewController: viewController)
                }
            }
        } else {
            // Otherwise present a local notification
            var notification = UILocalNotification()
            notification.alertBody = "คุณเริ่มเข้าใกล้พื้นที่เสี่ยงต่อการสูบบุหรี่"
            notification.soundName = "Default";
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        if region is CLCircularRegion {
            handleRegionEvent(region)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        if region is CLCircularRegion {
            handleRegionEvent(region)
        }
    }
    
    func notefromRegionIdentifier(identifier: String) -> String? {
        if let savedItems = NSUserDefaults.standardUserDefaults().arrayForKey(kSavedItemsKey) {
            for savedItem in savedItems {
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification {
                    if geotification.identifier == identifier {
                        return geotification.note
                    }
                }
            }
        }
        return nil
    }

    
}