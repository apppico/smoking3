//
//  Setting_User.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/7/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class Setting_User: UIViewController, UIActionSheetDelegate, UIAlertViewDelegate,UITextFieldDelegate {
    
    
    var id:String?
    var name:String?
    var gender:String?
    var age:String?
    var arr : [Int] = []
    
    //Action  Text Name --------------------------------------------------------------
    @IBOutlet weak var view_Backgroud: UIView!
    @IBOutlet weak var lb_Name: UILabel!
    @IBAction func ac_Username(sender: UIButton) {
        nameAction()
        sender.backgroundColor = UIColor.clearColor()
    }
    @IBAction func ac_Username_Touch(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    // Action  text  Gender -----------------------------------------------------------
    @IBOutlet weak var lb_Gender: UILabel!
    @IBAction func ac_Gender(sender: UIButton) {
            genderAction()
            sender.backgroundColor = UIColor.clearColor()
    }
    @IBAction func ac_Gender_Touch(sender: UIButton) {
          sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    // Action text Age  ------------------------------------------------------------
    @IBOutlet weak var lb_age: UILabel!
    @IBAction func ac_Age(sender: UIButton) {
        
         sender.backgroundColor = UIColor.clearColor()
        
        var actionSheet =  UIActionSheet()
        actionSheet.title = "Age(อายุ)"
        actionSheet.delegate = self
        
        for i in 13...100{
            arr.append(i)
            actionSheet.addButtonWithTitle("\(i)")
        }
        actionSheet.cancelButtonIndex = actionSheet.addButtonWithTitle("Cancel")
        actionSheet.showInView(UIApplication.sharedApplication().keyWindow)
    }
    @IBAction func ac_Age_Touch(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view_Backgroud.layer.borderWidth = 1.0
        view_Backgroud.layer.borderColor = UIColor(rgba: "#EEEEEE").CGColor
        view_Backgroud.clipsToBounds = true
        
        getDataJson()
        
        lb_Name.text = name
        lb_Gender.text = gender
        lb_age.text = age
        
        // Do any additional setup after loading the view.
    }
    
    func getDataJson(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Id")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            var res = results3[results3.count - 1] as! NSManagedObject
            var _userid = res.valueForKey("id") as? String
            var _username = res.valueForKey("name") as? String
            var _gender = res.valueForKey("gender") as? String
            var _age = res.valueForKey("age") as? String
            
            self.id = _userid
            self.name = _username
            self.gender = _gender
            self.age = _age
            
        }else{
            println("error")
        }
    }
    
    func nameAction(){
        
        var alert = UIAlertController(title: "ชื่อผู้ใช้", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = self.name!
            textField.placeholder = "Name"
        })
        alert.addAction(UIAlertAction(title: "ยกเลิก", style: .Default, handler: { (action) -> Void in
        }))
        alert.addAction(UIAlertAction(title: "ตกลง", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as! UITextField
            
            if textField.text != "" {
                self.lb_Name.text = textField.text
                self.name = textField.text
                self.updateName()
                
            }else{
                var alertView:UIAlertView = UIAlertView()
                alertView.title = "กรุณากรอกชื่อผู้ใช้"
                alertView.message = "-"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
 
    func genderAction(){
        
        let optionMenu = UIAlertController(title: "Gender", message: "กรุณาเลือกเพศ", preferredStyle: .ActionSheet)
        let deleteAction = UIAlertAction(title: "ชาย", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.lb_Gender.text = "ชาย"
            self.gender = self.lb_Gender.text
            self.updateGender()
        })
        let saveAction = UIAlertAction(title: "หญิง", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.lb_Gender.text = "หญิง"
            self.gender =  self.lb_Gender.text
            self.updateGender()
        })
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancel")
        })
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }

    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        if (buttonIndex == 89){
            println("Cancel")
        }else{
            while buttonIndex<88{
                switch buttonIndex{
                    case buttonIndex:
                    
                    lb_age.text = "\(buttonIndex+13)"
                    self.age = lb_age.text
                    self.updateAge()
                    
                    return  lb_age.text = "\(buttonIndex+13)"
                default:
                    return lb_age.text = ""
                }
            }
        }
    }
    
    func updateName(){
        
        if self.id != nil{
            let post = "&userId=\(self.id!)&userName=\(self.name!)"
            NSLog("PostData: %@",post);
            var url:NSURL = NSURL(string:"http://fahsai.bloodd.org/iquitsmoke/updateUserName.php")!
            var postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
            var postLength:NSString = String( postData.length )
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            var reponseError: NSError?
            var response: NSURLResponse?
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil ) {
                let res = response as! NSHTTPURLResponse!;
                
                if (res.statusCode >= 200 && res.statusCode < 300)
                {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    println("\(responseData)")
                    var error: NSError?
                    let jsonData = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary
                    
                    if( error != nil){
                        println("OK \(error!.localizedDescription)")
                        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                        var context:NSManagedObjectContext = appDel.managedObjectContext!
                        var newName = NSEntityDescription.insertNewObjectForEntityForName("Id", inManagedObjectContext: context) as! NSManagedObject
                        newName.setValue(self.id, forKey: "id")
                        newName.setValue(self.gender, forKey: "gender")
                        newName.setValue(self.name, forKey: "name")
                        newName.setValue(self.age, forKey: "age")
                        
                        context.save(nil)
                        println(newName)
                    }else{
                        println(error!.localizedDescription)
                    }
                }
            }
        }else{
            println("Error Id")
        }
        
    }
    
    func updateGender(){
        
        if self.id != nil{
            let post = "&userId=\(self.id!)&gender=\(self.gender!)"
            NSLog("PostData: %@",post);
            var url:NSURL = NSURL(string:"http://fahsai.bloodd.org/iquitsmoke/updateGender.php")!
            var postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
            var postLength:NSString = String( postData.length )
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            var reponseError: NSError?
            var response: NSURLResponse?
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil ) {
                let res = response as! NSHTTPURLResponse!;
                if (res.statusCode >= 200 && res.statusCode < 300)
                {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    println("\(responseData)")
                    var error: NSError?
                    let jsonData = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary
                    
                    if( error != nil){
                        println("OK \(error!.localizedDescription)")
                        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                        var context:NSManagedObjectContext = appDel.managedObjectContext!
                        var newName = NSEntityDescription.insertNewObjectForEntityForName("Id", inManagedObjectContext: context) as! NSManagedObject
                        newName.setValue(self.id, forKey: "id")
                        newName.setValue(self.gender, forKey: "gender")
                        newName.setValue(self.name, forKey: "name")
                        newName.setValue(self.age, forKey: "age")
                        context.save(nil)
                        println(newName)
                    }else{
                        println(error!.localizedDescription)
                    }
                }
            }
        }else{
            println("Error Id")
        }
    }
    
    func updateAge(){
        
        if self.id != nil{
            let post = "&userId=\(self.id!)&age=\(self.age!)"
            NSLog("PostData: %@",post);
            var url:NSURL = NSURL(string:"http://fahsai.bloodd.org/iquitsmoke/updateAge.php")!
            var postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
            var postLength:NSString = String( postData.length )
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            var reponseError: NSError?
            var response: NSURLResponse?
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil ) {
                let res = response as! NSHTTPURLResponse!;
                
                if (res.statusCode >= 200 && res.statusCode < 300){
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    println("\(responseData)")
                    var error: NSError?
                    let jsonData = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary
                    
                    if( error != nil){
                        println("OK \(error!.localizedDescription)")
                        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                        var context:NSManagedObjectContext = appDel.managedObjectContext!
                        var newName = NSEntityDescription.insertNewObjectForEntityForName("Id", inManagedObjectContext: context) as! NSManagedObject
                        newName.setValue(self.id, forKey: "id")
                        newName.setValue(self.gender, forKey: "gender")
                        newName.setValue(self.name, forKey: "name")
                        newName.setValue(self.age, forKey: "age")
                        context.save(nil)
                        println(newName)
                    }else{
                        println(error!.localizedDescription)
                    }
                }
            }else{
                println("Error Id")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
