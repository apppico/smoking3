//
//  CalendarSmokeing.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/14/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class CalendarSmokeing: UIViewController {
    
    @IBOutlet var lb_StartDay: UILabel!
    @IBOutlet var lb_EndDay: UILabel!
    @IBOutlet var lb_NoSmokDay: UILabel!
    @IBOutlet var lb_PercentDay: UILabel!
    @IBOutlet var btn_1: UIButton!
    @IBOutlet var btn_2: UIButton!
    @IBOutlet var btn_3: UIButton!
    
    var scrollView: UIScrollView!
    var containerView: UIView!
    
    var numDay = 0
    var _totalStart:String?
    var _totalEnd:String?
    
    var buttons : [TestClass] = []
    var totalAvtiveBtn : Int = 0
    var notSmokeCounter : Int = 0
    var smokeCounter : Int = 0
    var numTotalDay = 0
    
    var getDayString :String = ""
    var getStatusString:String = ""
    
    var postStatusString : [String] = []
    
    var strNoSmok : String?
    var strPercent : String?
    
    var stateText : Int!
    
    var inputString : [String] = []
    var intAraay:[String] = []
    
    var alertView: UIView!
    var animator: UIDynamicAnimator!
    var overlayView: UIView!
    
    var btn_Sender:UIButton!
    var day:String!
    var month:Int!
    var lbl_band:UILabel!
  
    
    var timerCount = 0
    var timerRunning = false
    var timer = NSTimer()
    var random:String!
    
    var num : [Int] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.scrollView = UIScrollView()
        self.scrollView.contentSize = self.view.bounds.size
        self.scrollView.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        self.containerView = UIView()
        self.scrollView.addSubview(self.containerView)
        self.view.addSubview(self.scrollView)
        
        btn_1.layer.cornerRadius = 3
        btn_1.layer.borderWidth = 1
        btn_1.layer.borderColor = UIColor(rgba: "#0277BD").CGColor
        btn_1.layer.backgroundColor = UIColor.clearColor().CGColor
        
        btn_2.layer.cornerRadius = 3
        btn_2.layer.borderWidth = 1
        btn_2.layer.borderColor = UIColor(rgba: "#F44336").CGColor
        btn_2.layer.backgroundColor = UIColor(rgba: "#F44336").CGColor
        
        btn_3.layer.cornerRadius = 3
        btn_3.layer.borderWidth = 1
        btn_3.layer.borderColor = UIColor(rgba: "#689F38").CGColor
        btn_3.layer.backgroundColor = UIColor(rgba: "#689F38").CGColor
        
        loadCalendar()
        
        if _totalStart != nil {
            
            lb_StartDay.text = _totalStart!
            lb_EndDay.text = _totalEnd!
            
        } else{
            println("NO DATA")
        }
        
        setup()
        
    }
    
    func setup(){
        
        isPostDataDay()
        isGetdataDay()
        initButtonData()
        buttonDay()
        setText()
        
        animator = UIDynamicAnimator(referenceView: view)
        createOverlay()
        
        timerRandom()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollView.frame = CGRect(x: 0, y: 260, width: self.view.frame.size.width, height: self.view.frame.size.height)
        containerView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height)
    }

    func setText(){
        
        self.strNoSmok = "\(notSmokeCounter)/\(totalAvtiveBtn)"
        lb_NoSmokDay.text =  self.strNoSmok
        
        let percent = (CGFloat(notSmokeCounter)/CGFloat(totalAvtiveBtn)) * 100
        var strPercentDay = NSString(format: "%.f", percent)
        self.strPercent = (strPercentDay  as String) + " %"
        lb_PercentDay.text =   self.strPercent
        
        checkData()
    }
    
    func initButtonData(){
        for (id,val) in enumerate(inputString){
            var isActive : Bool!
            var isActiveSmoke : Bool!
            var isActiveNoSmok : Bool!
            if intAraay[id] == "0"{
                isActive = false
            }else{
                isActive = true
                totalAvtiveBtn++
            }
            if postStatusString != [""] {
                
                if postStatusString[id] == "-1"{
                    isActiveSmoke = true
                }else{
                    isActiveSmoke = false
                }
                if postStatusString[id] == "1"{
                    isActiveNoSmok = true
                }else{
                    isActiveNoSmok = false
                }
            }else{
                isActiveSmoke = false
                isActiveNoSmok = false
            }
            buttons.append(TestClass(getDay: val, isActive: isActive, isActiveSmoke: isActiveSmoke, isActiveNoSmok: isActiveNoSmok, btnState: ButtonStatus.NotChoose))
            
        }
    }
    func buttonDay(){
        
        let padding: CGFloat = 10
        let width = self.view.frame.size.width / 7 - padding - (padding/7)
        var labelX: CGFloat = 10
        var buttonY: CGFloat = 10 // our Starting Offset, could be 0
        var buttonX: CGFloat = 10
        var buttonX1: CGFloat = 10
        var buttonX2: CGFloat = 10
        var buttonX3: CGFloat = 10
        
        for i in 1...7 {
            let dayLabel = UILabel(frame: CGRect(x: labelX, y: 235, width: width, height: 21))
            dayLabel.textAlignment = .Center
            dayLabel.textColor = UIColor.darkGrayColor()
            labelX = labelX + width + padding // we are going to space these UIButtons 50px apart
            switch i {
            case 1: dayLabel.text = "จ"
            case 2: dayLabel.text = "อ"
            case 3: dayLabel.text = "พ"
            case 4: dayLabel.text = "พฤ"
            case 5: dayLabel.text = "ศ"
            case 6: dayLabel.text = "ส"
            case 7: dayLabel.text = "อา"
            default: break
            }
            self.view.addSubview(dayLabel)
        }
        
        for (id,btn) in enumerate(buttons) {
            let d = btn.getDay[0].toInt()!
            let m = btn.getDay[1].toInt()!
            let y = btn.getDay[2].toInt()!
            
            
            let villainButton = UIButton(frame: CGRect(x: buttonY, y: 0, width: width, height: width))
            buttonY = buttonY + width + padding // we are going to space these UIButtons 50px apart
            
            villainButton.layer.cornerRadius = 3 // get some fancy pantsy rounding
            villainButton.backgroundColor = UIColor.clearColor()
            villainButton.setTitleColor(UIColor(rgba: "#6F7179"), forState: UIControlState.Normal)
            //          villainButton.setTitleColor(UIColor.brownColor(), forState: UIControlState.Highlighted)
            villainButton.layer.borderWidth = 1
            villainButton.layer.borderColor = UIColor(rgba: "#0277BD").CGColor
            villainButton.clipsToBounds = true
            villainButton.setTitle("\(d)", forState: UIControlState.Normal) // We are going to use the item name as the Button Title here.
            villainButton.titleLabel!.font = UIFont.systemFontOfSize(14)
            villainButton.titleLabel!.lineBreakMode = .ByTruncatingTail
            //            villainButton.titleLabel!.text = "\(d)/\(m)/\(y)"
            villainButton.titleLabel!.tag = m
            villainButton.tag = id
            
            numDay++
            
            let yBtn = width + padding
            
            if numDay >= 8   {
                villainButton.frame =  CGRectMake(buttonX, yBtn, width, width)
                buttonX = buttonX + width + padding
            }else{
                
            }
            if numDay >= 15 {
                villainButton.frame =  CGRectMake(buttonX1, yBtn*2, width, width)
                buttonX1 = buttonX1 + width + padding
                
            }else{
                
            }
            if numDay >= 22 {
                villainButton.frame = CGRectMake(buttonX2, yBtn*3, width, width)
                buttonX2 = buttonX2 + width + padding
            }else{
                
            }
            if numDay >= 29 {
                villainButton.frame = CGRectMake(buttonX3, yBtn*4, width, width)
                buttonX3 = buttonX3  + width + padding
            }else{
                
            }
            //วันที่ไม่อยู่ในโปรแกรม การเลิกบุหรี่
            if btn.isActive == true {
                villainButton.addTarget(self, action: "villainButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
                self.containerView.addSubview(villainButton)
                
            }else{
                self.containerView.addSubview(villainButton)
                villainButton.backgroundColor = UIColor(rgba: "#E1E1E1")
                villainButton.setTitleColor(UIColor(rgba: "#6F7179"), forState: UIControlState.Normal)
                villainButton.layer.borderColor = UIColor(rgba: "#E1E1E1").CGColor
            }
            
            if btn.isActiveSmoke == true{
                villainButton.backgroundColor = UIColor(rgba: "#F44336")
                villainButton.layer.borderColor = UIColor(rgba: "#F44336").CGColor
                btn.btnState = ButtonStatus.Smoked
                
                self.smokeCounter++
                self.setText()
            }else{
                
            }
            if btn.isActiveNoSmok == true{
                villainButton.backgroundColor = UIColor(rgba: "#689F38")
                villainButton.layer.borderColor = UIColor(rgba: "#689F38").CGColor
                btn.btnState = ButtonStatus.NotSmoke
                
                self.notSmokeCounter++
                self.setText()
            }else{
                
            }
        }
    }
    func villainButtonPressed(sender:UIButton!) {
        
        var dateDay = sender.titleLabel!.text!
    
        self.month =  sender.titleLabel!.tag
        
        var dateYears = String(sender.tag)
        
        self.btn_Sender = sender
  
        showAlert()

    }
    
    func randomText(){
        
        let result1 = myInstance.textRandom
        let randomIndex = Int(arc4random_uniform(UInt32(result1.count)))
        self.random = result1[randomIndex]
        
        if(self.lbl_band != nil){
            self.lbl_band.text = "\(result1[randomIndex])"
        }else{
            
        }
    }
    
    
    func timerRandom(){
        
        if timerRunning == false{
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("Counting"), userInfo: nil, repeats: true)
            timerRunning = true
        }
    }
    
    func Counting(){
        timerCount += 1
        if (timerCount == 10){
            timerCount = 0
            randomText()
            timerRunning = true
        }else{
            timerRunning = false
        }
    }
    
    func createAlert(){
        
        let alertWidth: CGFloat = view.frame.width
        let alertHeight: CGFloat = view.frame.height
        let alertViewFrame: CGRect = CGRectMake(0, 0, alertWidth, alertHeight)
        alertView = UIView(frame: alertViewFrame)
        alertView.backgroundColor = UIColor.whiteColor()
        
        let lbl_brand = UILabel(frame: CGRectMake(10, alertHeight/5, alertWidth-20, alertHeight/4))
        lbl_brand.textAlignment = NSTextAlignment.Center
        lbl_brand.numberOfLines = 0
        lbl_brand.textColor = UIColor.blackColor()
        lbl_brand.font = UIFont(name: "Avenir", size: 18)
        
        let result1 = myInstance.textRandom
        let randomIndex = Int(arc4random_uniform(UInt32(result1.count)))
        self.lbl_band = lbl_brand
        
        lbl_brand.text = "\(result1[randomIndex])"

        let lbl_date = UILabel(frame: CGRectMake(0, alertHeight/2+alertWidth/5.5, alertWidth, 20))
        lbl_date.textAlignment = NSTextAlignment.Center
        lbl_date.textColor = UIColor.blackColor()
        lbl_date.font = UIFont(name: "Avenir", size: 18)
        lbl_date.text = "วันที่ \(self.btn_Sender.titleLabel!.text!) \(myInstance.calendarMonth[self.month-1])"
        
        
        let lbl_Smoking = UILabel(frame: CGRectMake(0, alertHeight/2+alertWidth/4, alertWidth, 20))
        lbl_Smoking.textAlignment = NSTextAlignment.Center
        lbl_Smoking.textColor = UIColor.blackColor()
        lbl_Smoking.font = UIFont(name: "Avenir", size: 18)
        lbl_Smoking.text = "คุณสูบบุหรี่หรือไม่?"
        
        alertView.addSubview(lbl_brand)
        alertView.addSubview(lbl_date)
        alertView.addSubview(lbl_Smoking)
        
        let btnCancel = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        btnCancel.titleLabel?.font = UIFont(name: "Avenir-Black", size: 25)
        btnCancel.setTitle("ไม่สูบ", forState: UIControlState.Normal)
        btnCancel.setBackgroundImage(UIImage(named: "ic_not_smok"), forState: UIControlState.Normal)
        btnCancel.contentVerticalAlignment = UIControlContentVerticalAlignment.Bottom
        btnCancel.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btnCancel.backgroundColor  = UIColor(rgba: "#689F38")
        btnCancel.frame = CGRectMake(0, alertHeight-alertHeight/3.5, alertWidth/2, alertHeight/3.5)
        btnCancel.addTarget(self, action: Selector("CancelAlert"), forControlEvents: UIControlEvents.TouchUpInside)
        btnCancel.clipsToBounds = true
        
    
        let btnSmoking = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        btnSmoking.titleLabel?.font = UIFont(name: "Avenir-Black", size: 25)
        btnSmoking.setTitle("สูบ", forState: UIControlState.Normal)
        btnSmoking.setBackgroundImage(UIImage(named: "ic_dang"), forState: UIControlState.Normal)
        btnSmoking.contentVerticalAlignment = UIControlContentVerticalAlignment.Bottom
        btnSmoking.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btnSmoking.backgroundColor = UIColor(rgba: "#F44336")
        btnSmoking.frame = CGRectMake(alertWidth/2, alertHeight-alertHeight/3.5, alertWidth/2, alertHeight/3.5)
        btnSmoking.addTarget(self, action: Selector("DoneAlert"), forControlEvents: UIControlEvents.TouchUpInside)
        btnSmoking.clipsToBounds = true

        alertView.addSubview(btnSmoking)
        alertView.addSubview(btnCancel)
        view.addSubview(alertView)
        
        
    }
    
    func CancelAlert() {
        
        animator.removeAllBehaviors()
        var gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [alertView])
        gravityBehaviour.gravityDirection = CGVectorMake(0.0, 10.0);
        animator.addBehavior(gravityBehaviour)
        
        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        var itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [alertView])
        itemBehaviour.addAngularVelocity(CGFloat(-M_PI_2), forItem: alertView)
        animator.addBehavior(itemBehaviour)
        
        
        UIView.animateWithDuration(0.4, animations: {
            self.overlayView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.alertView.removeFromSuperview()
                self.alertView = nil
        })
        addNoSmok()
    }
    func DoneAlert() {
        
        animator.removeAllBehaviors()
        var gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [alertView])
        gravityBehaviour.gravityDirection = CGVectorMake(0.0, 10.0)
        animator.addBehavior(gravityBehaviour)
        
        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        var itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [alertView])
        itemBehaviour.addAngularVelocity(CGFloat(-M_PI_2), forItem: alertView)
        animator.addBehavior(itemBehaviour)
        
        UIView.animateWithDuration(0.4, animations: {
            self.overlayView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.alertView.removeFromSuperview()
                self.alertView = nil
        })
        addSmoking()
        
    }
    
//    func smokAfterWaking_up(){
//        let optionMenu = UIAlertController(title: "สูบบุหรี่", message: "หลังตื่นนอน", preferredStyle: .ActionSheet)
//        for i in 1...60{
//            
//            num.append(i)
//            let smokeing = UIAlertAction(title: "\(i)", style: .Default, handler: {
//                (alert: UIAlertAction!) -> Void in
//                println("\(i)")
//                self.addSmoking()
//
//            })
//            optionMenu.addAction(smokeing)
//        }
//        let total = UIAlertAction(title: "มากกว่า 60", style: .Default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            println("มากกว่า 60")
//            self.addSmoking()
//
//        })
//        optionMenu.addAction(total)
//        self.presentViewController(optionMenu, animated: true, completion: nil)
//        
//    }
    
    func addSmoking(){
        
        self.btn_Sender.backgroundColor = UIColor(rgba: "#F44336")
            self.btn_Sender.layer.borderColor = UIColor(rgba: "#F44336").CGColor
                let state = self.buttons[ self.btn_Sender.tag].btnState!
                    switch state{
                    case ButtonStatus.NotSmoke :
                        self.notSmokeCounter--
                        self.smokeCounter++
        
                    case ButtonStatus.Smoked : break
                    case ButtonStatus.NotChoose :
                        self.smokeCounter++
        
                    }
                    self.buttons[self.btn_Sender.tag].btnState = ButtonStatus.Smoked
                    self.setText()
                    self.loopPrint()
  
    }
    
    
    func addNoSmok(){
        self.btn_Sender.backgroundColor = UIColor(rgba: "#689F38")
        self.btn_Sender.layer.borderColor = UIColor(rgba: "#689F38").CGColor
                    let state = self.buttons[ self.btn_Sender.tag].btnState!
                    switch state{
                    case ButtonStatus.Smoked :
                        self.notSmokeCounter++
                        self.smokeCounter--
        
                    case ButtonStatus.NotSmoke : break
                    case ButtonStatus.NotChoose :
                        self.notSmokeCounter++
        
                    }
                    self.buttons[ self.btn_Sender.tag].btnState = ButtonStatus.NotSmoke
                    self.setText()
                    self.loopPrint()
        
    }
    
    func createOverlay() {
        // Create a gray view and set its alpha to 0 so it isn't visible
        overlayView = UIView(frame: view.bounds)
        overlayView.alpha = 0.0
        view.addSubview(overlayView)
    }
    
    func showAlert() {
        if (alertView == nil) {
            createAlert()
        }
        animator.removeAllBehaviors()
        // Animate in the overlay
        UIView.animateWithDuration(0.4) {
            self.overlayView.alpha = 1.0
        }
        // Animate the alert view using UIKit Dynamics.
        alertView.alpha = 1.0
        var snapBehaviour: UISnapBehavior = UISnapBehavior(item: alertView, snapToPoint: view.center)
        animator.addBehavior(snapBehaviour)
    }

    
    

    func isCompleteCollectData() -> Bool{
        
        let sum = notSmokeCounter + smokeCounter
        if sum == totalAvtiveBtn {
            return true
        }
        return false
    }
    func loopPrint(){
        
        var dayString : String = ""
        var dayStatus : String = ""
        
        for (id,btn) in enumerate(buttons){
            switch btn.btnState!{
            case ButtonStatus.Smoked : stateText = -1
            case ButtonStatus.NotSmoke : stateText = 1
            case ButtonStatus.NotChoose : stateText = 0
            }
            
            dayString += "\(btn.getDay[0])/\(btn.getDay[1])/\(btn.getDay[2]),"
            dayStatus += "\(stateText),"
        }

        
        self.getStatusString = dayStatus
        isGetdataDay()
    }
    func isGetdataDay() {
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var newName = NSEntityDescription.insertNewObjectForEntityForName("Calen_total", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue(self.getStatusString, forKey: "isCheckStatus")
        context.save(nil)
        
        println(newName)
        
        isPostDataDay()
    }
    func isPostDataDay(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Calen_total")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            
            var postStatus = res.valueForKey("isCheckStatus") as? String
    
            self.getStatusString = postStatus!
            
        }
        jsonDataStatus()
    }
    
    func jsonDataStatus(){
        
        var fullDayArr = self.getStatusString.componentsSeparatedByString(",")
        var postStatus:[String] = []
        
        for status in fullDayArr{
            postStatus += [status]
        }
        self.postStatusString = postStatus
        println("\(self.postStatusString)")
        
    }
    
    func loadCalendar(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Calendar")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            
            var startDay = res.valueForKey("start_Day") as? String
            var end_Day = res.valueForKey("end_Day") as? String
            var total_Day = res.valueForKey("total_Day") as? String
            var inprogram = res.valueForKey("inprogram") as? String
            
                self._totalStart = startDay!
                self._totalEnd = end_Day!
                self.inputString = total_Day!.componentsSeparatedByString(",")
                self.intAraay = inprogram!.componentsSeparatedByString(",")
          }
        
    }
    
    func checkData() {
        
        if self.notSmokeCounter == self.totalAvtiveBtn {
            let appDomain = NSBundle.mainBundle().bundleIdentifier
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)
            
            self.performSegueWithIdentifier("YesSmok", sender: self)
            
                isCompleteCollectData() == false
            
        }else{
            
            if isCompleteCollectData(){
                let appDomain = NSBundle.mainBundle().bundleIdentifier
                NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)
                self.performSegueWithIdentifier("NoSmok", sender: self)
            }else{
                isCompleteCollectData() == false
            }
        }  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
}



