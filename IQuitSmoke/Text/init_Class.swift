


//
//  init_Class.swift
//  ไทยไร้ควัน
//
//  Created by Apinat Jitrat on 11/9/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

var dra:[tabbarController] = []

class tabbarController{
    
    var drawerController:KYDrawerController?
    
    init(drawerController:KYDrawerController){
        self.drawerController = drawerController
    }
}

//Add Smoking
var dataClass : [addDataSmok] = []

class addDataSmok{
    var img_ : UIImageView!
    var name_: String!
    var type_: String!
    var pric_: String!
    
    init (img_:UIImageView,name_:String,type_:String,pric_:String){
        self.img_ = img_
        self.name_ = name_
        self.type_ = type_
        self.pric_ = pric_
        
    }
}

//Add Button
enum ButtonStatus{
    case Smoked
    case NotSmoke
    case NotChoose
}

class TestClass {
    
    var getDay : [String]!
    var isActive : Bool!
    var isActiveSmoke : Bool!
    var isActiveNoSmok : Bool!
    var btnState : ButtonStatus!
    
    init(getDay : String, isActive: Bool,isActiveSmoke : Bool, isActiveNoSmok: Bool, btnState: ButtonStatus) {
        self.getDay = getDay.componentsSeparatedByString("/")
        self.isActive = isActive
        self.isActiveSmoke = isActiveSmoke
        self.isActiveNoSmok = isActiveNoSmok
        self.btnState = btnState
        
    }
}

//Places
var place:[places] = []

class places{
    var name_t:String!
    var text:String!
    var calendar:String!
    var image:UIImage!
    
    init (name_t:String,text:String,calendar:String,image:UIImage){
        self.name_t = name_t
        self.text = text
        self.calendar = calendar
        self.image = image
    }
}

//Recomment
var recommen:[recomment] = []

class recomment {
    var title:String!
    var dec:String!
    var image:UIImage!
    
    init(title:String,dec:String,image:UIImage){
        self.title = title
        self.dec = dec
        self.image = image
    }
}

//loadJsonSmok
var loadJsonSmoking:[loadJsonSmok] = []

class loadJsonSmok{
    var dataJsomSmok:NSMutableArray!
    
    init(dataJsomSmok:NSMutableArray){
        self.dataJsomSmok = dataJsomSmok
    }
}

//add textName
var addDataTextName:[addDataTextFiledName] = []

class addDataTextFiledName{
    var name:String!
    
    init(name:String){
        self.name = name
    }
}

//add textFiledAge
var addDataTextAge:[addDataTextFiledAge] = []

class addDataTextFiledAge {
    var nameAge:String!
    
    init(nameAge:String){
        self.nameAge = nameAge
    }
}

//add textFiledGender
var addDataTextGender:[addDataTextFiledGender] = []

class addDataTextFiledGender {
    var nameGender:String!
    
    init(nameGender:String){
        self.nameGender = nameGender
    }
}

//add textFiledGender
var addDataTextDiease:[addDataTextFiledDiease] = []

class addDataTextFiledDiease {
    var nameDiease:String!
    
    init(nameDiease:String){
        self.nameDiease = nameDiease
    }
}

