//
//  UIColorExtension.swift
//  ThaiPBS Plus
//
//  Created by iAmMac on 8/20/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit


enum ColorIntensity{
    case _050
    case _100
    case _200
    case _300
    case _400
    case _500
    case _600
    case _700
    case _800
    case _900
    case A100
    case A200
    case A400
    case A700
}

extension UIColor {

    
    convenience init(rgba: String) {
//
//  UIColorExtension.swift
//  RSBarcodesSample
//
//  Created by R0CKSTAR on 6/13/14.
//  Copyright (c) 2014 P.D.Q. All rights reserved.
//

        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if rgba.hasPrefix("#") {
            let index   = advance(rgba.startIndex, 1)
            let hex     = rgba.substringFromIndex(index)
            let scanner = NSScanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexLongLong(&hexValue) {
                switch (count(hex)) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    //                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    //                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    //                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    //                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                    alpha  = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    red    = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    green  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    blue   = CGFloat(hexValue & 0x000000FF)         / 255.0
                default: break
                    //                    print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", appendNewline: false)
                }
            } else {
                //                print("Scan hex error")
            }
        } else {
            //            print("Invalid RGB string, missing '#' as prefix", appendNewline: false)
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    class func blackMain1() -> UIColor{
        return UIColor(rgba: "#313131")
    }

    class func orangeTPBSColor() -> UIColor{
        return UIColor(rgba: "#EF5122")
    }
    
    
    class func redColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#ffebee")
        case ._100 :  return UIColor(rgba: "#ffcdd2")
        case ._200 :  return UIColor(rgba: "#ef9a9a")
        case ._300 :  return UIColor(rgba: "#e57373")
        case ._400 :  return UIColor(rgba: "#ef5350")
        case ._500 :  return UIColor(rgba: "#f44336")
        case ._600 :  return UIColor(rgba: "#e53935")
        case ._700 :  return UIColor(rgba: "#d32f2f")
        case ._800 :  return UIColor(rgba: "#c62828")
        case ._900 :  return UIColor(rgba: "#b71c1c")
        case .A100 :  return UIColor(rgba: "#ff8a80")
        case .A200 :  return UIColor(rgba: "#ff5252")
        case .A400 :  return UIColor(rgba: "#ff1744")
        case .A700 :  return UIColor(rgba: "#d50000")
        }
    }
    class func pinkColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fce4ec")
        case ._100 :  return UIColor(rgba: "#f8bbd0")
        case ._200 :  return UIColor(rgba: "#f48fb1")
        case ._300 :  return UIColor(rgba: "#f06292")
        case ._400 :  return UIColor(rgba: "#ec407a")
        case ._500 :  return UIColor(rgba: "#e91e63")
        case ._600 :  return UIColor(rgba: "#d81b60")
        case ._700 :  return UIColor(rgba: "#c2185b")
        case ._800 :  return UIColor(rgba: "#ad1457")
        case ._900 :  return UIColor(rgba: "#880e4f")
        case .A100 :  return UIColor(rgba: "#ff80ab")
        case .A200 :  return UIColor(rgba: "#ff4081")
        case .A400 :  return UIColor(rgba: "#f50057")
        case .A700 :  return UIColor(rgba: "#c51162")
        }
    }
    
    class func purpleColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#f3e5f5")
        case ._100 :  return UIColor(rgba: "#e1bee7")
        case ._200 :  return UIColor(rgba: "#ce93d8")
        case ._300 :  return UIColor(rgba: "#ba68c8")
        case ._400 :  return UIColor(rgba: "#ab47bc")
        case ._500 :  return UIColor(rgba: "#9c27b0")
        case ._600 :  return UIColor(rgba: "#8e24aa")
        case ._700 :  return UIColor(rgba: "#7b1fa2")
        case ._800 :  return UIColor(rgba: "#6a1b9a")
        case ._900 :  return UIColor(rgba: "#4a148c")
        case .A100 :  return UIColor(rgba: "#ea80fc")
        case .A200 :  return UIColor(rgba: "#e040fb")
        case .A400 :  return UIColor(rgba: "#d500f9")
        case .A700 :  return UIColor(rgba: "#aa00ff")
        }
    }
    
    class func deepPurpleColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#ede7f6")
        case ._100 :  return UIColor(rgba: "#d1c4e9")
        case ._200 :  return UIColor(rgba: "#b39ddb")
        case ._300 :  return UIColor(rgba: "#9575cd")
        case ._400 :  return UIColor(rgba: "#7e57c2")
        case ._500 :  return UIColor(rgba: "#673ab7")
        case ._600 :  return UIColor(rgba: "#5e35b1")
        case ._700 :  return UIColor(rgba: "#512da8")
        case ._800 :  return UIColor(rgba: "#4527a0")
        case ._900 :  return UIColor(rgba: "#311b92")
        case .A100 :  return UIColor(rgba: "#b388ff")
        case .A200 :  return UIColor(rgba: "#7c4dff")
        case .A400 :  return UIColor(rgba: "#651fff")
        case .A700 :  return UIColor(rgba: "#6200ea")
        }
    }
    
    class func indigoColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e8eaf6")
        case ._100 :  return UIColor(rgba: "#c5cae9")
        case ._200 :  return UIColor(rgba: "#9fa8da")
        case ._300 :  return UIColor(rgba: "#7986cb")
        case ._400 :  return UIColor(rgba: "#5c6bc0")
        case ._500 :  return UIColor(rgba: "#3f51b5")
        case ._600 :  return UIColor(rgba: "#3949ab")
        case ._700 :  return UIColor(rgba: "#303f9f")
        case ._800 :  return UIColor(rgba: "#283593")
        case ._900 :  return UIColor(rgba: "#1a237e")
        case .A100 :  return UIColor(rgba: "#8c9eff")
        case .A200 :  return UIColor(rgba: "#536dfe")
        case .A400 :  return UIColor(rgba: "#3d5afe")
        case .A700 :  return UIColor(rgba: "#304ffe")
        }
    }
    
    class func blueColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e3f2fd")
        case ._100 :  return UIColor(rgba: "#bbdefb")
        case ._200 :  return UIColor(rgba: "#90caf9")
        case ._300 :  return UIColor(rgba: "#64b5f6")
        case ._400 :  return UIColor(rgba: "#42a5f5")
        case ._500 :  return UIColor(rgba: "#2196f3")
        case ._600 :  return UIColor(rgba: "#1e88e5")
        case ._700 :  return UIColor(rgba: "#1976d2")
        case ._800 :  return UIColor(rgba: "#1565c0")
        case ._900 :  return UIColor(rgba: "#0d47a1")
        case .A100 :  return UIColor(rgba: "#82b1ff")
        case .A200 :  return UIColor(rgba: "#448aff")
        case .A400 :  return UIColor(rgba: "#2979ff")
        case .A700 :  return UIColor(rgba: "#2962ff")
        }
    }
    class func lightBlueColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e1f5fe")
        case ._100 :  return UIColor(rgba: "#b3e5fc")
        case ._200 :  return UIColor(rgba: "#81d4fa")
        case ._300 :  return UIColor(rgba: "#4fc3f7")
        case ._400 :  return UIColor(rgba: "#29b6f6")
        case ._500 :  return UIColor(rgba: "#03a9f4")
        case ._600 :  return UIColor(rgba: "#039be5")
        case ._700 :  return UIColor(rgba: "#0288d1")
        case ._800 :  return UIColor(rgba: "#0277bd")
        case ._900 :  return UIColor(rgba: "#01579b")
        case .A100 :  return UIColor(rgba: "#80d8ff")
        case .A200 :  return UIColor(rgba: "#40c4ff")
        case .A400 :  return UIColor(rgba: "#00b0ff")
        case .A700 :  return UIColor(rgba: "#0091ea")
        }
    }
    class func cyanColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e0f7fa")
        case ._100 :  return UIColor(rgba: "#b2ebf2")
        case ._200 :  return UIColor(rgba: "#80deea")
        case ._300 :  return UIColor(rgba: "#4dd0e1")
        case ._400 :  return UIColor(rgba: "#26c6da")
        case ._500 :  return UIColor(rgba: "#00bcd4")
        case ._600 :  return UIColor(rgba: "#00acc1")
        case ._700 :  return UIColor(rgba: "#0097a7")
        case ._800 :  return UIColor(rgba: "#00838f")
        case ._900 :  return UIColor(rgba: "#006064")
        case .A100 :  return UIColor(rgba: "#84ffff")
        case .A200 :  return UIColor(rgba: "#18ffff")
        case .A400 :  return UIColor(rgba: "#00e5ff")
        case .A700 :  return UIColor(rgba: "#00b8d4")
        }
    }
    
    class func tealColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e0f2f1")
        case ._100 :  return UIColor(rgba: "#b2dfdb")
        case ._200 :  return UIColor(rgba: "#80cbc4")
        case ._300 :  return UIColor(rgba: "#4db6ac")
        case ._400 :  return UIColor(rgba: "#26a69a")
        case ._500 :  return UIColor(rgba: "#009688")
        case ._600 :  return UIColor(rgba: "#00897b")
        case ._700 :  return UIColor(rgba: "#00796b")
        case ._800 :  return UIColor(rgba: "#00695c")
        case ._900 :  return UIColor(rgba: "#004d40")
        case .A100 :  return UIColor(rgba: "#a7ffeb")
        case .A200 :  return UIColor(rgba: "#64ffda")
        case .A400 :  return UIColor(rgba: "#1de9b6")
        case .A700 :  return UIColor(rgba: "#00bfa5")
        }
    }
    class func greenColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#e8f5e9")
        case ._100 :  return UIColor(rgba: "#c8e6c9")
        case ._200 :  return UIColor(rgba: "#a5d6a7")
        case ._300 :  return UIColor(rgba: "#81c784")
        case ._400 :  return UIColor(rgba: "#66bb6a")
        case ._500 :  return UIColor(rgba: "#4caf50")
        case ._600 :  return UIColor(rgba: "#43a047")
        case ._700 :  return UIColor(rgba: "#388e3c")
        case ._800 :  return UIColor(rgba: "#2e7d32")
        case ._900 :  return UIColor(rgba: "#1b5e20")
        case .A100 :  return UIColor(rgba: "#b9f6ca")
        case .A200 :  return UIColor(rgba: "#69f0ae")
        case .A400 :  return UIColor(rgba: "#00e676")
        case .A700 :  return UIColor(rgba: "#00c853")
        }
    }
    class func lightGreenColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#f1f8e9")
        case ._100 :  return UIColor(rgba: "#dcedc8")
        case ._200 :  return UIColor(rgba: "#c5e1a5")
        case ._300 :  return UIColor(rgba: "#aed581")
        case ._400 :  return UIColor(rgba: "#9ccc65")
        case ._500 :  return UIColor(rgba: "#8bc34a")
        case ._600 :  return UIColor(rgba: "#7cb342")
        case ._700 :  return UIColor(rgba: "#689f38")
        case ._800 :  return UIColor(rgba: "#558b2f")
        case ._900 :  return UIColor(rgba: "#33691e")
        case .A100 :  return UIColor(rgba: "#ccff90")
        case .A200 :  return UIColor(rgba: "#b2ff59")
        case .A400 :  return UIColor(rgba: "#76ff03")
        case .A700 :  return UIColor(rgba: "#64dd17")
        }
    }
    class func limeColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#f9fbe7")
        case ._100 :  return UIColor(rgba: "#f0f4c3")
        case ._200 :  return UIColor(rgba: "#e6ee9c")
        case ._300 :  return UIColor(rgba: "#dce775")
        case ._400 :  return UIColor(rgba: "#d4e157")
        case ._500 :  return UIColor(rgba: "#cddc39")
        case ._600 :  return UIColor(rgba: "#c0ca33")
        case ._700 :  return UIColor(rgba: "#afb42b")
        case ._800 :  return UIColor(rgba: "#9e9d24")
        case ._900 :  return UIColor(rgba: "#827717")
        case .A100 :  return UIColor(rgba: "#f4ff81")
        case .A200 :  return UIColor(rgba: "#eeff41")
        case .A400 :  return UIColor(rgba: "#c6ff00")
        case .A700 :  return UIColor(rgba: "#aeea00")
        }
    }
    class func yellowColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fffde7")
        case ._100 :  return UIColor(rgba: "#fff9c4")
        case ._200 :  return UIColor(rgba: "#fff59d")
        case ._300 :  return UIColor(rgba: "#fff176")
        case ._400 :  return UIColor(rgba: "#ffee58")
        case ._500 :  return UIColor(rgba: "#ffeb3b")
        case ._600 :  return UIColor(rgba: "#fdd835")
        case ._700 :  return UIColor(rgba: "#fbc02d")
        case ._800 :  return UIColor(rgba: "#f9a825")
        case ._900 :  return UIColor(rgba: "#f57f17")
        case .A100 :  return UIColor(rgba: "#ffff8d")
        case .A200 :  return UIColor(rgba: "#ffff00")
        case .A400 :  return UIColor(rgba: "#ffea00")
        case .A700 :  return UIColor(rgba: "#ffd600")
        }
    }
    class func amberColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fff8e1")
        case ._100 :  return UIColor(rgba: "#ffecb3")
        case ._200 :  return UIColor(rgba: "#ffe082")
        case ._300 :  return UIColor(rgba: "#ffd54f")
        case ._400 :  return UIColor(rgba: "#ffca28")
        case ._500 :  return UIColor(rgba: "#ffc107")
        case ._600 :  return UIColor(rgba: "#ffb300")
        case ._700 :  return UIColor(rgba: "#ffa000")
        case ._800 :  return UIColor(rgba: "#ff8f00")
        case ._900 :  return UIColor(rgba: "#ff6f00")
        case .A100 :  return UIColor(rgba: "#ffe57f")
        case .A200 :  return UIColor(rgba: "#ffd740")
        case .A400 :  return UIColor(rgba: "#ffc400")
        case .A700 :  return UIColor(rgba: "#ffab00")
        }
    }
    
    class func orangeColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fff3e0")
        case ._100 :  return UIColor(rgba: "#ffe0b2")
        case ._200 :  return UIColor(rgba: "#ffcc80")
        case ._300 :  return UIColor(rgba: "#ffb74d")
        case ._400 :  return UIColor(rgba: "#ffa726")
        case ._500 :  return UIColor(rgba: "#ff9800")
        case ._600 :  return UIColor(rgba: "#fb8c00")
        case ._700 :  return UIColor(rgba: "#f57c00")
        case ._800 :  return UIColor(rgba: "#ef6c00")
        case ._900 :  return UIColor(rgba: "#e65100")
        case .A100 :  return UIColor(rgba: "#ffd180")
        case .A200 :  return UIColor(rgba: "#ffab40")
        case .A400 :  return UIColor(rgba: "#ff9100")
        case .A700 :  return UIColor(rgba: "#ff6d00")
        }
    }
    
    class func deepOrangeColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fbe9e7")
        case ._100 :  return UIColor(rgba: "#ffccbc")
        case ._200 :  return UIColor(rgba: "#ffab91")
        case ._300 :  return UIColor(rgba: "#ff8a65")
        case ._400 :  return UIColor(rgba: "#ff7043")
        case ._500 :  return UIColor(rgba: "#ff5722")
        case ._600 :  return UIColor(rgba: "#f4511e")
        case ._700 :  return UIColor(rgba: "#e64a19")
        case ._800 :  return UIColor(rgba: "#d84315")
        case ._900 :  return UIColor(rgba: "#bf360c")
        case .A100 :  return UIColor(rgba: "#ff9e80")
        case .A200 :  return UIColor(rgba: "#ff6e40")
        case .A400 :  return UIColor(rgba: "#ff3d00")
        case .A700 :  return UIColor(rgba: "#dd2c00")
        }
    }
    
    class func brownColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#efebe9")
        case ._100 :  return UIColor(rgba: "#d7ccc8")
        case ._200 :  return UIColor(rgba: "#bcaaa4")
        case ._300 :  return UIColor(rgba: "#a1887f")
        case ._400 :  return UIColor(rgba: "#8d6e63")
        case ._500 :  return UIColor(rgba: "#795548")
        case ._600 :  return UIColor(rgba: "#6d4c41")
        case ._700 :  return UIColor(rgba: "#5d4037")
        case ._800 :  return UIColor(rgba: "#4e342e")
        case ._900 :  return UIColor(rgba: "#3e2723")
        default    :  return UIColor(rgba: "#795548")
        }
    }
    
    class func grayColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#fafafa")
        case ._100 :  return UIColor(rgba: "#f5f5f5")
        case ._200 :  return UIColor(rgba: "#eeeeee")
        case ._300 :  return UIColor(rgba: "#e0e0e0")
        case ._400 :  return UIColor(rgba: "#bdbdbd")
        case ._500 :  return UIColor(rgba: "#9e9e9e")
        case ._600 :  return UIColor(rgba: "#757575")
        case ._700 :  return UIColor(rgba: "#616161")
        case ._800 :  return UIColor(rgba: "#424242")
        case ._900 :  return UIColor(rgba: "#212121")
        default    :  return UIColor(rgba: "#9e9e9e")
        }
    }
    
    class func blueGreyColor(intentsity : ColorIntensity) -> UIColor{
        
        switch (intentsity){
        case ._050 :  return UIColor(rgba: "#eceff1")
        case ._100 :  return UIColor(rgba: "#cfd8dc")
        case ._200 :  return UIColor(rgba: "#b0bec5")
        case ._300 :  return UIColor(rgba: "#90a4ae")
        case ._400 :  return UIColor(rgba: "#78909c")
        case ._500 :  return UIColor(rgba: "#607d8b")
        case ._600 :  return UIColor(rgba: "#546e7a")
        case ._700 :  return UIColor(rgba: "#455a64")
        case ._800 :  return UIColor(rgba: "#37474f")
        case ._900 :  return UIColor(rgba: "#263238")
        default    :  return UIColor(rgba: "#607d8b")
        }
    }    
    
}
