//
//  TabbarViewPage.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/21/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class TabbarViewPage: UIViewController {
    
    var swiftPagesView:SwiftPages!
    var drawerController:KYDrawerController?

    override func viewDidLoad() {
        super.viewDidLoad()

        //Instantiation and the setting of the size and position
        
        viewScore()
    
    }
    
    func viewScore(){
        
        swiftPagesView = SwiftPages(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
        
        //Initiation
        var VCIDs : [String] = ["SecondVC","FirstVC"]
        var buttonTitles : [String] = ["คำแนะนำ","สถานที่ปลอดบุหรี่"]
        
        swiftPagesView.enableBarShadow(true)
        swiftPagesView.setButtonsTextColor(UIColor.whiteColor())
        swiftPagesView.setAnimatedBarColor(UIColor.whiteColor())
        swiftPagesView.initializeWithVCIDsArrayAndButtonTitlesArray(VCIDs, buttonTitlesArray: buttonTitles) //ชื่อ
        swiftPagesView.setTopBarBackground(UIColor(rgba: "#0277BD"))
        swiftPagesView.setAnimatedBarColor(UIColor(red: 255/255, green: 250/255, blue: 205/255, alpha: 1.0))
        
         self.view.addSubview(swiftPagesView)
    }


    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
}
