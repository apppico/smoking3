//
//  ClinicDataView.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/19/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import MapKit

class ClinicDataView: UIViewController,MKMapViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var lb_name: UILabel!
    @IBOutlet var lb_place: UILabel!
    @IBOutlet var mapView: MKMapView!

    @IBAction func btn_ActionMap(sender: UIButton) {
      openMapView()
        sender.backgroundColor = UIColor.clearColor()
    }
    @IBAction func btn_DownMap(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    var str_Name:String?
    var str_Place:String?
    var str_Tel:String?
    var str_Cell:String?
    var str_Map:String?
    
    var getMap:[String]!
    var lat:Double!
    var lang:Double!
    var getTel:[String]!
    var getSizeCellTel:[String]!
    var nameTel:String = ""
    var nameTelCell:String = ""
    var buttonY:CGFloat = 339
    var labelY:CGFloat = 343
    var nameVV:String!
    
    var addressToLinkTo = ""

    
    @IBOutlet var layout_Map: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize.height = 690
        
        layer()
        getCelling()
        getLocationMap()
    }
    
    private func layer(){
        title = str_Name
        lb_name.text = str_Name
        lb_place.text = str_Place
    }
    
    func getCelling(){
        
        if (str_Tel != nil){

        self.getTel = str_Tel!.componentsSeparatedByString(",")
   
        for var i=1; i<=self.getTel.count; i++ {
            if i != self.getTel.count{
                nameTel += "\(self.getTel[i-1])\n"
            }else{
                nameTel += "\(self.getTel[i-1])"
            }
            nameVV = self.getTel[i-1]
            buttonAction()
            labelAction()
        }
                        
        }else{
            
        }
    }

    func buttonAction(){
        
        let button   = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        button.frame = CGRectMake(326, buttonY, 30, 30)
        buttonY += 35
        button.backgroundColor = UIColor.clearColor()
        button.setBackgroundImage(UIImage(named: "ic_calll"), forState: UIControlState.Normal)
        button.layer.cornerRadius = button.frame.size.width/2
        button.layer.borderWidth = 1.5
        button.titleLabel!.text = nameVV
        button.layer.borderColor =  UIColor(rgba: "#0277BD").CGColor
        button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        button.addTarget(self, action: "buttonDown:", forControlEvents: UIControlEvents.TouchDown)

        button.clipsToBounds = true
        self.scrollView.addSubview(button)
    }
  
    func labelAction(){
        
        var label = UILabel(frame: CGRectMake(50, labelY, 200, 21))
        labelY += 35
        label.textAlignment = NSTextAlignment.Left
        label.text = nameVV
        layout_Map.constant = CGFloat(1 * labelY)
        self.scrollView.addSubview(label)
    }
    
    func buttonAction(sender:UIButton!){
        sender.backgroundColor = UIColor.clearColor()

        var dateDay = sender.titleLabel!.text!
        
        let cellingClinic = dateDay.componentsSeparatedByString("-")
        var str:String = ""
        for var i = 0 ; i < cellingClinic.count; i++  {
            str += cellingClinic[i]
        }
        var cell = str.componentsSeparatedByString(" ")

        UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(cell[0])")!)
        println("\(cell[0])")

    }
    
    func buttonDown(sender:UIButton){
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    
    func getLocationMap(){
        
        self.getMap = str_Map!.componentsSeparatedByString(",")
        self.lat = (self.getMap[0] as NSString).doubleValue
        self.lang = (self.getMap[1] as NSString).doubleValue
        
        getMapView()
    }
    
    func getMapView(){
        
        var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        var mylocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat,lang)
        var myRegion:MKCoordinateRegion = MKCoordinateRegionMake(mylocation, theSpan)
        self.mapView.setRegion(myRegion, animated: true)
        var empire = MKPointAnnotation()
        empire.coordinate = mylocation
        
        self.mapView.addAnnotation(empire)
    }
    
    func openMapView(){
       
        self.addressToLinkTo = "http://maps.apple.com/?q=111 Some place drive, Oak Ridge \(self.str_Map!)"
        
        self.addressToLinkTo = self.addressToLinkTo.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let url = NSURL(string: self.addressToLinkTo)
        UIApplication.sharedApplication().openURL(url!)
        
    }
   
}
