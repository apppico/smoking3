//
//  MasterViewController.swift
//  CustomUITableViewCell
//
//  Created by Debasis Das on 12/26/14.
//  Copyright (c) 2014 Knowstack. All rights reserved.
//

import UIKit
import CoreData



class MasterViewController: UITableViewController, UITableViewDataSource {
    
    @IBOutlet var activity_Indicator: UIActivityIndicatorView!
    let kURL = "http://fahsai.bloodd.org/iquitsmoke/getRecommendation.php"
    let mainQueue = dispatch_get_main_queue()
    let diffQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

    var daten : NSMutableArray!{
        didSet{
            tableView.reloadData()
        }
    }
   
    var images = [String:UIImage?]()
    
    var title_name:String?
    var img_name = [String:UIImage?]()
    var dec_name:String?
    var datener = []
    
    var drawerController:KYDrawerController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "คำแนะนำ"
        if  isDatabaseEmpty() {
            loadJSON()
        }else{
            loadDatabase()
        }
        
//        var refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: Selector("sortArray"), forControlEvents: UIControlEvents.ValueChanged)
//        self.refreshControl = refreshControl
        
    }
    
//    func sortArray() {
//        
//        loadJSON()
//        dispatch_async(self.mainQueue){
//            self.tableView.reloadData()
//        }
//          refreshControl?.endRefreshing()
//    }
    
    
    private func loadJSON(){
        
        let request = NSURLRequest(URL: NSURL(string: kURL)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            response, data, error in
            
            if (error != nil) {
                println(error!.localizedDescription)
                return
            }
            
            dispatch_async(self.diffQueue){
                // wird in anderer Queue ausfuhrt ->
                var error : NSError?

                if let data = (NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as! NSDictionary)["items"] as? NSMutableArray{
                    self.daten = data
                    self.saveDatabase()
                    
                }else{
                    if (error != nil) {
                        println(error?.localizedDescription)
                    }
                }
                
                dispatch_async(self.mainQueue){
                    self.tableView.reloadData()
                }
            }
            self.activity_Indicator.startAnimating()

        })
        

    }
    
    private func saveDatabase(){
        
        if self.daten != nil{
            
            for dic in self.daten{
                let url = dic["image"] as! String
                    let data = NSData(contentsOfURL: NSURL(string: url)!)
                    var image:UIImage?
                if (data != nil){
                    image = UIImage(data: data!)
                    self.images["\(self.daten.indexOfObject(dic))"] = image
                }else{
                    
                }
                let _title = dic["title"] as! String
                let _description = dic["description"] as! String
               
                
                
                //Add data Entities //File Model_data //Set AppDelegate
                
                var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                var context:NSManagedObjectContext = appDel.managedObjectContext!
                
                var newName = NSEntityDescription.insertNewObjectForEntityForName("Recomment", inManagedObjectContext: context) as! NSManagedObject
                
                newName.setValue(_title, forKey: "title")
                newName.setValue(image, forKey: "image")
                newName.setValue(_description, forKey: "dection")
                
                
                context.save(nil)
                
                println(newName)
                
            }
            self.activity_Indicator.stopAnimating()
        }
    }
    
    
    private func isDatabaseEmpty() -> Bool{
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Recomment")
        fetch2.returnsObjectsAsFaults = false
        
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                
                println("11111Database is empty !!!")
                
                
            }else{
                
                let res = x[0]
                let url = res.valueForKey("image") as? String
                
                return false
            }
            
        }else{
            println("Not found anything")
        }
        
        return true

    }

    private func loadDatabase() -> Bool{
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Recomment")
        fetch2.returnsObjectsAsFaults = false
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                println("Database is empty !!!")
            }else{
         
                self.datener = context2.executeFetchRequest(fetch2, error: nil)!
                    for dic in self.datener{
                    
                        var img1 = dic.valueForKey("image") as? UIImage
            
                        self.img_name["\(self.datener.indexOfObject(dic))"] = img1
                }

                return false
            }
            
        }else{
            println("Not found anything")
        }
        
        return true

    }
    
    
    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if daten != nil{
            return daten.count
        }
        return datener.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        if daten != nil{
                let _title = (daten[indexPath.row] as! NSDictionary)["title"] as? String
                let _description = (daten[indexPath.row] as! NSDictionary)["description"] as? String
            
                (cell.contentView.viewWithTag(10) as! UILabel).text = _title
                (cell.contentView.viewWithTag(20) as! UILabel).text = _description
            
                if (images["\(indexPath.row)"] != nil){
                
                (cell.contentView.viewWithTag(30) as! UIImageView).image = images["\(indexPath.row)"]!
                (cell.contentView.viewWithTag(30) as! UIImageView).layer.cornerRadius = 1
                (cell.contentView.viewWithTag(30) as! UIImageView).clipsToBounds = true
                
                }
            }
        
        else{
            let _title = (datener[indexPath.row]).valueForKey("title") as? String
            let _description = (datener[indexPath.row]).valueForKey("dection") as? String
    
            (cell.contentView.viewWithTag(10) as! UILabel).text = _title
            (cell.contentView.viewWithTag(20) as! UILabel).text = _description
    
            if (img_name["\(indexPath.row)"] != nil){
    
            (cell.contentView.viewWithTag(30) as! UIImageView).image = img_name["\(indexPath.row)"]!
            (cell.contentView.viewWithTag(30) as! UIImageView).layer.cornerRadius = 1
            (cell.contentView.viewWithTag(30) as! UIImageView).clipsToBounds = true
    
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        var title_:String!
        var description_:String!
        var image:UIImage!
        var index = indexPath.row

         if daten != nil{
            
            title_ = (daten[indexPath.row] as! NSDictionary)["title"] as? String
            description_ = (daten[indexPath.row] as! NSDictionary)["description"] as? String
            
            if (images["\(indexPath.row)"] != nil){
                image = images["\(indexPath.row)"]!
            }
        }
          else{
        
            title_ = (datener[indexPath.row]).valueForKey("title") as? String
            description_ = (datener[indexPath.row]).valueForKey("dection") as? String
        
            if (img_name["\(indexPath.row)"] != nil){
                image = img_name["\(indexPath.row)"]!
            }
        }
        
        recommen.append(recomment(title: title_!, dec: description_!, image: image!))
        connextNavigation()
    }

    func connextNavigation(){

        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainRecomment()
        }
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

    
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
    


}

