
//
//  Calendar_NoSmoked.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/28/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class Calendar_NoSmoked: UIViewController {
    
    var _dayTotal : Int? = 0
    var allWeek : Int?
    var allDay : Int?
    
    @IBAction func btn_ActionSmoked(sender: UIButton) {
        
        calendarBtn()
        isGetdataDay()
        
        self.performSegueWithIdentifier("start_Smoked", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
 
    
    func getDayOfWeek(today:String)->Int {
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "d/M/yyyy"
        let todayDate = formatter.dateFromString(today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
        let dayOfWeek = (myComponents.weekday - myCalendar.firstWeekday)
        
        return dayOfWeek
    }
    func calendarBtn(){
       
            //function วันที่ปัจจุบัน----------------------------------------
            
            let date1 = NSDate()
            let formatter = NSDateFormatter()
            formatter.timeStyle = .ShortStyle
            formatter.stringFromDate(date1)
            let dateFormat1 : NSDateFormatter = NSDateFormatter()
            dateFormat1.dateFormat = "d/M/yyyy"
            
            let datastart = dateFormat1.stringFromDate(date1)
            //เงื่อนไข---------------------------------------------------
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "d/M/yyyy"
        
            //FUNCTION กำหนดวันไม่เกิน 28 วันจากวันปัจจุบัน
            let dateFormat_1 : NSDateFormatter = NSDateFormatter()
            dateFormat_1.dateFormat = "d"
            let dateDay = dateFormat_1.stringFromDate(date1)
        
            let dateFormat2 : NSDateFormatter = NSDateFormatter()
            dateFormat2.dateFormat = "M"
            let datemonth = dateFormat2.stringFromDate(date1)
        
        
            let dateFormat3 : NSDateFormatter = NSDateFormatter()
            dateFormat3.dateFormat = "yyyy"
            let dateyear = dateFormat3.stringFromDate(date1)
        
            //ข้อมูลที่จะเก็บไว้ในดาต้าเบส
            let dataDMY : NSDateFormatter = NSDateFormatter()
            dataDMY.dateFormat = "d/M/yyyy"
            let dataTotalDMY = dataDMY.stringFromDate(date1)
        
            var day_1:Int! = dateDay.toInt()
            var month1:Int! = datemonth.toInt()
            var year1:Int! = dateyear.toInt()
        
            let dateComponents = NSDateComponents()
        
            dateComponents.day = day_1 + 27
            dateComponents.month = month1
            dateComponents.year = year1
        
            var date = NSCalendar.currentCalendar().dateFromComponents(dateComponents)
        
           let dateFormat : NSDateFormatter = NSDateFormatter()
            dateFormat.dateFormat = "d/M/yyyy"
        
            let dataend = dateFormat.stringFromDate(date!)
        
            let date_ = NSDateFormatter()
            date_.dateFormat = "EEEE"
            let dayOfWeekString = date_.stringFromDate(date!)
            println("วันสุดท้าย \(dayOfWeekString)")
        
            //  แปลงข้อมูลจาก String ให้เป็น Date
        
            let startDate:NSDate = dateFormatter.dateFromString(datastart)!
            let endDate:NSDate = dateFormatter.dateFromString(dataend)!
            
            let cal = NSCalendar.currentCalendar()
        
            let unit:NSCalendarUnit = .CalendarUnitDay
            
            // fuction นับจำนวนวัน
            let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: nil)
            
            
            let day = components.day + 1
            let day1 = "\(day) วัน"
            
            self._dayTotal = day
            // function นับจำนวน ว่าวันที่เริ่ม เป็นสัปดาห์ที่เท่าไหร่ของปี
            let calendar = NSCalendar.currentCalendar();
            calendar.firstWeekday = 2;
            
            
            var weekNum_Start = calendar.components(NSCalendarUnit.CalendarUnitWeekOfYear, fromDate: startDate).weekOfYear;
            var weekNum_End = calendar.components(NSCalendarUnit.CalendarUnitWeekOfYear, fromDate: endDate).weekOfYear;
            
            println("weekStart\(weekNum_Start) weekEnd\(weekNum_End)")
        
            //-------------------------
            let weekday_start = self.getDayOfWeek(datastart)
            println("วันที่\(weekday_start)ของสัปดาห์")
            
            let weekday_end = self.getDayOfWeek(dataend)
            println("วันที่\(weekday_end)ของสัปดาห์")
            // วันเริ่มต้นของสัปดาห์ ----------------------------------------------------
            
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let currentDateComponents = myCalendar.components(.CalendarUnitYearForWeekOfYear | .CalendarUnitWeekOfYear , fromDate: startDate)
            
            let startOfWeek = cal.dateFromComponents(currentDateComponents)
            
            let dateFormat_week_start : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start.dateFormat = "d"
            let date_st_d = dateFormat_week_start.stringFromDate(startOfWeek!)
            
            let dateFormat_week_start1 : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start1.dateFormat = "M"
            let date_st_m = dateFormat_week_start1.stringFromDate(startOfWeek!)
            
            let dateFormat_week_start2 : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start2.dateFormat = "yyyy"
            let date_st_y = dateFormat_week_start2.stringFromDate(startOfWeek!)
            
            var Day:Int! = date_st_d.toInt()
            var Moth:Int! = date_st_m.toInt()
            var Year:Int! = date_st_y.toInt()
            
            
            if(weekNum_End >= weekNum_Start){
                self.allWeek = (weekNum_End - weekNum_Start) + 1
            }else{
                self.allWeek = (52 - (weekNum_Start - weekNum_End)) + 1
            }
            
            self.allDay = self.allWeek! * 7
            
            println("\(self.allDay!) วันทั้งหมด")
            
            var str : String = ""
            var inprogram : String = ""
            
            
            for(var i=1; i <= self.allDay; i++){
                let dateCom_ = NSDateComponents()
                
                dateCom_.day = Day + i
                dateCom_.month = Moth
                dateCom_.year = Year
                
                var someDate: NSDate = NSCalendar.currentCalendar().dateFromComponents(dateCom_)!
                
                let dateForma_ = NSDateFormatter()
                dateForma_.dateFormat = "d/M/yyyy"
                
                let total_ = dateForma_.stringFromDate(someDate)
                
                
                if ((startDate.compare(someDate) == NSComparisonResult.OrderedAscending) || (startDate.compare(someDate) == NSComparisonResult.OrderedSame)) && ((endDate.compare(someDate) == NSComparisonResult.OrderedDescending) || (endDate.compare(someDate) == NSComparisonResult.OrderedSame)){
                    
                    inprogram += "\(1),"
                    
                }else{
                    inprogram += "\(0),"
                    
                }
                
                if i != self.allDay{
                    str += "\(total_),"
                    
                }else{
                    str += "\(total_)"
                    
                }
                
            }
            
            println(str)
            println(inprogram)
            // upload เข้า data เครื่อง
            var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            var context:NSManagedObjectContext = appDel.managedObjectContext!
            
            var newName = NSEntityDescription.insertNewObjectForEntityForName("Calendar", inManagedObjectContext: context) as! NSManagedObject
            newName.setValue("" + day1, forKey: "id_Day")
            newName.setValue("" + datastart, forKey: "start_Day")
            newName.setValue("" + dataend, forKey: "end_Day")
            newName.setValue("" + str, forKey: "total_Day")
            newName.setValue("" + inprogram, forKey: "inprogram")
            
            context.save(nil)
            
            println(newName)
    }
    
    func isGetdataDay() {
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var newName = NSEntityDescription.insertNewObjectForEntityForName("Calen_total", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue("", forKey: "isCheckStatus")
        context.save(nil)
        
        println(newName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
