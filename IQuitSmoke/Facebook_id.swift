//
//  Facebook_id.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/24/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(Facebook_id)

class Facebook_id: NSManagedObject {
    
    @NSManaged var email_Fx:String
    @NSManaged var profile_Fx:String
    @NSManaged var id_facebook:String
    
   
}
