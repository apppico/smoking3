//
//  MainViewStoryboard.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/12/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData

class MainView: UIViewController {

    var mainView:String?
    
    var viewController = "gotoViewController"
    var MainViewController = "gotoMenuProfile"
    
    let mURL = "http://fahsai.bloodd.org/iquitsmoke/getCigarette.php"
    let mmainQueue = dispatch_get_main_queue()
    let mdiffQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    var dataSmokJson = loadJsonSmoking

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMain()
        noticaitionCalendar()
        noticaitionRandom()
        loadJSON()
        
        if (mainView == "1"){            
             self.performSegueWithIdentifier(MainViewController, sender: self)
        }else{
            self.performSegueWithIdentifier(viewController, sender: self)
        }
        println("\(mainView)")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func noticaitionCalendar(){
        let dateCom_ = NSDateComponents()
        var getYear = dateCom_.year
        var getMonth = dateCom_.month
        var getDay = dateCom_.day
        var getHour:Int! = 6
        var getMinute:Int! = 00
        
        dateCom_.year = getYear
        dateCom_.month = getMonth
        dateCom_.day = getDay
        dateCom_.hour = getHour
        dateCom_.minute = getMinute
        dateCom_.timeZone = NSTimeZone.defaultTimeZone()
        
        var calender: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var date: NSDate = calender.dateFromComponents(dateCom_)!
        var localNotification: UILocalNotification = UILocalNotification()
        localNotification.fireDate = date
        localNotification.alertBody = "วันนี้คุณสูบบุหรี่หรือไม่"
        localNotification.category = "SMOKING"
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        localNotification.repeatInterval = NSCalendarUnit.CalendarUnitDay
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
    }
    
    func noticaitionRandom(){
        let dateCom_ = NSDateComponents()
        var getYear = dateCom_.year
        var getMonth = dateCom_.month
        var getDay = dateCom_.day
        var getHour:Int! = 12
        var getMinute:Int! = 00
        let result1 = myInstance.textRandom
        let randomIndex = Int(arc4random_uniform(UInt32(result1.count)))
        
        dateCom_.year = getYear
        dateCom_.month = getMonth
        dateCom_.day = getDay
        dateCom_.hour = getHour
        dateCom_.minute = getMinute
        dateCom_.timeZone = NSTimeZone.defaultTimeZone()
        
        var calender: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var date: NSDate = calender.dateFromComponents(dateCom_)!
        var localNotification: UILocalNotification = UILocalNotification()
        localNotification.fireDate = date
        localNotification.alertBody = "\(result1[randomIndex])"
        localNotification.category = "RANDOM"
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        localNotification.repeatInterval = NSCalendarUnit.CalendarUnitDay
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
    }
    
    
    func loadMain(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "MainHome")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            self.mainView = res.valueForKey("nameMain") as? String
                     
        }else{
            println("NODATAJSON")
            
        }
        
    }
    
    
   func loadJSON(){
        let request = NSURLRequest(URL: NSURL(string: mURL)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            response, data, error in
            
            if (error != nil) {
                println(error!.localizedDescription)
                return
            }
            
            dispatch_async(self.mdiffQueue){
                // wird in anderer Queue ausfuhrt ->
                var error : NSError?
                
                if let data = (NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as! NSDictionary)["Cigarette"] as? NSMutableArray{
                    loadJsonSmoking.append(loadJsonSmok(dataJsomSmok: data))
                }else{
                    if (error != nil) {
                        println(error?.localizedDescription)
                        loadJsonSmoking.append(loadJsonSmok(dataJsomSmok: NSMutableArray()))

                    }
                  
                }
            }
        })
    }
    
}
