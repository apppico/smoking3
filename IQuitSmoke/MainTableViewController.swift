//
//  MainTableViewController.swift
//  ไทยไร้ควัน
//
//  Created by Somjintana K. on 23/01/2016.
//  Copyright (c) 2016 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class MainTableViewController: UITableViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate {
    
    let mURL = "http://fahsai.bloodd.org/iquitsmoke/getRisky.php"
    let mmainQueue = dispatch_get_main_queue()
    let mdiffQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    
    var daten : NSMutableArray! {
        didSet {
            self.collectionView?.reloadData()
            self.tableView.reloadData()
        }
    }
    
    var drawerController:KYDrawerController?
    var images = [String:UIImage?]()
    var title_name:String?
    var img_name = [String:UIImage?]()
    var dec_name:String?
    var datener = []
    var id:String?
    var yearSmok:CGFloat?  //สูบมาแล้วกี่ปี
    var smokAmount:CGFloat? //สูบวันละกี่ม้วนต่อวัน
    var costStr:CGFloat? //ราคาบุหรี่
    
    var timerCount = 0
    var timerRunning = false
    var timer = NSTimer()
    
    let COLLECTION_PADDING : CGFloat = 16.0

    @IBOutlet weak var image_Photo: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_age: UILabel!
    @IBOutlet var lb_Gender: UILabel!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var smokeButton: UIButton!
    
    @IBOutlet var viewAge: UIView!
    @IBOutlet var lb_AgeSmok: UILabel!
    @IBOutlet var text_Day: UILabel!
    @IBOutlet var lb_ageView: UILabel!
    
    @IBOutlet var viewPrice: UIView!
    @IBOutlet var lb_PriceSmok: UILabel!
    @IBOutlet var text_Price: UILabel!
    @IBOutlet var lb_priceView: UILabel!
    
    @IBOutlet var scrolView: UIScrollView!
    @IBOutlet var pageView: UIPageControl!
    @IBOutlet var lb_nameDisease: UILabel!
    
    @IBOutlet var lb_TextSmoked: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isDatabaseEmpty() {
            loadJSON()
        } else {
            loadDatabase()
        }
        
        dispatch_async(self.mmainQueue){
            self.getDataID()
        }
        
        self.setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Set up
    
    func setupView() {
        self.title = "ข้อมูลส่วนตัว"
        
        image_Photo.layer.cornerRadius = image_Photo.frame.size.width/2
        image_Photo.layer.masksToBounds = true
//        image_Photo.layer.borderColor = UIColor(rgba: "#B20277BD").CGColor
//        image_Photo.layer.borderWidth = 2.0
        
//        photoButton.layer.cornerRadius = photoButton.frame.size.width/2
//        photoButton.layer.masksToBounds = true
//        photoButton.backgroundColor = UIColor(rgba: "#ffffff")
//        photoButton.layer.borderColor = UIColor(rgba: "#B20277BD").CGColor
//        photoButton.layer.borderWidth = 2.0
//        
//        smokeButton.layer.cornerRadius = smokeButton.frame.size.width/2
//        smokeButton.layer.masksToBounds = true
//        smokeButton.backgroundColor = UIColor(rgba: "#ffffff")
//        smokeButton.layer.borderColor = UIColor(rgba: "#B20277BD").CGColor
//        smokeButton.layer.borderWidth = 5.0
        
        colorView()
        borderText()
        slideImage()
        loadResultSmok()
        loadImage()
        calMoneyLoss()
    }
    
    func colorView(){
        viewAge.backgroundColor = UIColor(rgba: "#FFEBEE")
        viewPrice.backgroundColor = UIColor(rgba: "#FFEBEE")
        lb_ageView.backgroundColor = UIColor(rgba: "#B71C1C")
        lb_ageView.textColor = UIColor.whiteColor()
        lb_priceView.backgroundColor = UIColor(rgba: "#B71C1C")
        lb_priceView.textColor = UIColor.whiteColor()
        
        lb_PriceSmok.textColor = UIColor(rgba: "#B71C1C")
        lb_AgeSmok.textColor = UIColor(rgba: "#B71C1C")
        text_Day.textColor = UIColor(rgba: "#B71C1C")
        text_Price.textColor = UIColor(rgba: "#B71C1C")
    }
    
    func borderText(){
        let border_name1 = CALayer()
        let width_name1 = CGFloat(2.0)
        border_name1.borderColor = UIColor.blackColor().CGColor
        border_name1.frame = CGRect(x: 0, y: lb_TextSmoked.frame.size.height - width_name1, width:  self.view.frame.size.width, height: lb_TextSmoked.frame.size.height)
        
        border_name1.borderWidth = width_name1
        lb_TextSmoked.layer.addSublayer(border_name1)
        lb_TextSmoked.layer.masksToBounds = true
    }
    
    func slideImage() {
        let scrollViewWidth:CGFloat = self.scrolView.frame.width
        let scrollViewHeight:CGFloat = self.scrolView.frame.height
        
        for id in 1...8 {
            var image = UIImageView()
            if (id == 1){
                image.frame =  CGRectMake(0, 0,scrollViewWidth, scrollViewHeight)
                image.image = UIImage(named: "Disease\(id)")
            }else{
                image.frame = CGRectMake(scrollViewWidth * CGFloat(id-1), 0,scrollViewWidth, scrollViewHeight)
                image.image = UIImage(named: "Disease\(id)")
            }
            self.scrolView.addSubview(image)
        }
        
        lb_nameDisease.backgroundColor = UIColor(rgba: "#B20277BD")
        lb_nameDisease.textColor = UIColor.whiteColor()
        lb_nameDisease.text = "มะเร็งปอด"
        self.scrolView.contentSize = CGSizeMake(scrollViewWidth * 8, scrollViewHeight)
        self.scrolView.delegate = self
        self.pageView.currentPage = 0
    }
    
    
    // MARK: - Data source
    private func isDatabaseEmpty() -> Bool{
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        let fetch2 = NSFetchRequest(entityName: "Rissmok")
        fetch2.returnsObjectsAsFaults = false
        
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                println("11111Database is empty !!!")
            }else{
                let res = x[0]
                let url = res.valueForKey("image") as? String
                return false
            }
        } else {
            println("Not found anything")
        }
        return true
    }
    
    private func loadJSON(){
        let request = NSURLRequest(URL: NSURL(string: mURL)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            response, data, error in
            
            if (error != nil) {
                println(error!.localizedDescription)
                return
            }
            
            dispatch_async(self.mdiffQueue){
                // wird in anderer Queue ausfuhrt ->
                var error : NSError?
                
                if let data = (NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as! NSDictionary)["items"] as? NSMutableArray {
                    
                    self.daten = data
                    self.saveDatabase()
                }else{
                    if (error != nil) {
                        println(error?.localizedDescription)
                    }
                }
                dispatch_async(self.mmainQueue){
                    self.collectionView?.reloadData()
                    self.tableView.reloadData()
                }
            }
            //self.activityInd.startAnimating()
        })
    }
    
    private func saveDatabase() {
        if self.daten != nil {
            for dic in self.daten {
                let url = dic["image"] as! String
                let data = NSData(contentsOfURL: NSURL(string: url)!)
                let image = UIImage(data: data!)
                self.images["\(self.daten.indexOfObject(dic))"] = image
                
                let _title = dic["title"] as! String
                
                var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                var context:NSManagedObjectContext = appDel.managedObjectContext!
                
                var newName = NSEntityDescription.insertNewObjectForEntityForName("Rissmok", inManagedObjectContext: context) as! NSManagedObject
                newName.setValue(_title, forKey: "textname")
                newName.setValue(image, forKey: "image")
                context.save(nil)
                
                println(newName)
            }
        }
    }
    
    private func loadDatabase() -> Bool {
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        let fetch2 = NSFetchRequest(entityName: "Rissmok")
        fetch2.returnsObjectsAsFaults = false
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                println("Database is empty !!!")
            }else{
                
                self.datener = context2.executeFetchRequest(fetch2, error: nil)!
                
                for dic in self.datener {
                    var img1 = dic.valueForKey("image") as? UIImage
                    self.img_name["\(self.datener.indexOfObject(dic))"] = img1
                }
                return false
            }
        }else{
            println("Not found anything")
        }
        return true
    }
    
    func getDataID(){
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Id")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            
            var _userid = res.valueForKey("id") as? String
            var _username = res.valueForKey("name") as? String
            var _age = res.valueForKey("age") as? String
            var _gender = res.valueForKey("gender") as? String
            
            self.id = _userid!
            self.lb_name.text = " " + _username!
            self.lb_age.text = "\(_age!) ปี"
            self.lb_Gender.text = _gender!
            
            if (_gender == "ชาย" || _gender == " ชาย"){
                self.lb_Gender.textColor = UIColor(rgba: "#2196F3")
            }else{
                self.lb_Gender.textColor = UIColor(rgba: "#E91E63")
            }
            
        }else{
            println("NOT GETName")
        }
    }
    
    func loadImage(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Image")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            var _userimage = res.valueForKey("imgProfile") as? NSData
            
            if _userimage != nil{
                
                let image = UIImage(data: _userimage!)
                self.image_Photo.image = image
                
            }else{
                println("Error Image")
            }
            
        }else{
            println("NODATAJSON")
            
        }
        
    }
    
    private func loadResultSmok(){
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Smoking")
        fetch2.returnsObjectsAsFaults = false
        var results2:NSArray = context2.executeFetchRequest(fetch2, error: nil)!
        
        if(results2.count > 0){
            
            var res = results2[results2.count - 1] as! NSManagedObject
            var smok_1 = res.valueForKey("smok_year") as? String
            var smok_2 = res.valueForKey("smok_roll") as? String
            
            self.yearSmok = CGFloat((smok_1! as NSString).floatValue)
            self.smokAmount = CGFloat((smok_2! as NSString).floatValue)
            
            //            lb_numberSmok.text = "\(smok_2!)"
            
            calAgeLoss()
            loadDataSmok()
            calMoneyLoss()
        }else{
            println("NOT DATASMOK")
            
        }
    }
    
    private func loadDataSmok(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "AddSmok")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            var priceSmok = res.valueForKey("priceSmok") as? String
            self.costStr =  CGFloat((priceSmok! as NSString).floatValue)
            
        }else{
            println("NODATAPRICE")
        }
    }
    
    private func calMoneyLoss(){
        var costPerUnit:CGFloat?
        var money:CGFloat?
        
        if (costStr != nil){
            costPerUnit = costStr! / 20
            
            money = yearSmok! * 365 * smokAmount! * costPerUnit!
            
            self.lb_PriceSmok.text = ("\(money!)")
            self.text_Price.text = "บาท"
        }else if(yearSmok != nil){
            money = yearSmok! * 365 * smokAmount! * 5
            self.lb_PriceSmok.text = ("\(money!)")
            self.text_Price.text = "บาท"
            
        }
    }
    
    private func calAgeLoss(){
        
        var allMinute:CGFloat?
        var result:String?
        var textData:String!
        
        var year = 0
        var month = 0
        var day = 0
        
        if (yearSmok != nil){
            
            allMinute = yearSmok! * 365 * smokAmount! * 7
            
            while (allMinute! / 525600 >= 1){
                var h : CGFloat? = allMinute! % 525600
                year += 1
                allMinute! -= 525600
            }
            while(allMinute! / 43200 >= 1){
                month += 1
                allMinute! -= 43200
            }
            while(allMinute! / 1440 >= 1){
                day += 1
                allMinute! -= 1440
            }
            if (day>0){
                result = ("\(day)")
                textData = "วัน"
                self.text_Day.textAlignment = .Center
            }
            if (month>0){
                result = ("\(month)  \(day)")
                textData = "เดือน      วัน"
                self.text_Day.textAlignment = .Center
            }
            if (year>0){
                result = ("\(year)   \(month)")
                textData = "ปี       เดือน"
                self.text_Day.textAlignment = .Center
            }
            
        }
        self.text_Day.text = textData
        self.lb_AgeSmok.text = result!
    }
    
    func addJson(){
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newName = NSEntityDescription.insertNewObjectForEntityForName("Image", inManagedObjectContext: context) as! NSManagedObject
        
        let imageData = UIImageJPEGRepresentation(image_Photo.image, 0)
        
        newName.setValue(imageData, forKey: "imgProfile")
        context.save(nil)
    }
    
    func myImageUploadRequest() {
        let myUrl = NSURL(string: "http://fahsai.bloodd.org/iquitsmoke/updateProfilePicture.php")
        let request = NSMutableURLRequest(URL:myUrl!)
        request.HTTPMethod = "POST"
        let param = ["user_id" : self.id!]
        let boundary = generateBoundaryString()
        let imageData = UIImageJPEGRepresentation(image_Photo.image, 0)
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if (imageData == nil)  { return; }
        
        request.HTTPBody = createBodyWithParameters(param, filePathKey: "uploaded_file", imageDataKey: imageData, boundary: boundary)
        
        //activityInd.startAnimating()
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            if error != nil {
                println("error=\(error)")
                return
            }
            // You can print out response object
            println("******* response = \(response)")
            // Print out reponse body
            let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("****** response data = \(responseString!)")
            
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err) as? NSDictionary
            
            dispatch_async(dispatch_get_main_queue(),{
                //self.activityInd.stopAnimating()
            })
        }
        task.resume()
    }
    
    // MARK: - Action
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
    
    @IBAction func camera_Action(sender: AnyObject) {
        actionImage_Camera()
    }
    
    @IBAction func smoking_Action(sender: UIButton) {
        connextNavigartion()
    }
    
    func actionImage_Camera() {
        var openPic = UIAlertController(title: "เลือกรูปภาพ", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        let image = UIAlertAction(title: "แกลลอรี่", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            var photoPicker = UIImagePickerController()
            photoPicker.delegate = self
            photoPicker.sourceType = .PhotoLibrary
            self.presentViewController(photoPicker, animated: true, completion: nil)
            
        })
        let camera = UIAlertAction(title: "กล้อง", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
                //load the camera interface
                var picker = UIImagePickerController()
                picker.sourceType = UIImagePickerControllerSourceType.Camera
                picker.delegate = self
                picker.allowsEditing = false
                self.presentViewController(picker, animated: true, completion: nil)
            }else{
                //no camera available
                var alert = UIAlertController(title: "Error", message: "There is no camera available", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction)in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
        })
        let cancelAction = UIAlertAction(title: "ยกเลิก", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            println("Cancelled")
            
        })
        openPic.addAction(camera)
        openPic.addAction(cancelAction)
        openPic.addAction(image)
        
        self.presentViewController(openPic, animated: true, completion: nil)
    }
    
    func connextNavigartion() {
        if (dra.count > 0) {
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        } else {
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainLa_device()
        } else {
            
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        image_Photo.layer.cornerRadius = image_Photo.frame.size.width/2
        image_Photo.layer.masksToBounds = true
        image_Photo.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismissViewControllerAnimated(false, completion: nil)
        
        addJson()
        myImageUploadRequest()
        
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        var pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        var currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/6)/pageWidth)+1
        // Change the indicator
        self.pageView.currentPage = Int(currentPage)
        
        switch Int(currentPage){
        case 0 : lb_nameDisease.text = "มะเร็งปอด"
        case 1 : lb_nameDisease.text = "มะเร็งกล่องเสียง"
        case 2 : lb_nameDisease.text = "มะเร็งในลำคอ"
        case 3 : lb_nameDisease.text = "ถุงลมโป่งพอง"
        case 4 : lb_nameDisease.text = "หอบหืด"
        case 5 : lb_nameDisease.text = "หลอดเลือดในสมองแตก"
        case 6 : lb_nameDisease.text = "หลอดเลือดหัวใจตีบ"
        case 7 : lb_nameDisease.text = "ความดันโลหิตสูง"
        default : println("ScrollView")
            
        }
    }
    
    // MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if daten != nil {
            print("numberOfItemsInSection: daten \(daten.count)")
            return daten.count
        }
        print("numberOfItemsInSection: datener \(datener.count)")
        return datener.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: colvwCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! colvwCell
        
        if daten != nil {
            
            //self.activityInd.stopAnimating()
            
            cell.lblCell.text = (daten[indexPath.row] as! NSDictionary)["title"] as? String
            cell.lblCell.backgroundColor = UIColor(rgba: "#B20277BD")
            
            if (images["\(indexPath.row)"] != nil){
                
                cell.imgCell.image = images["\(indexPath.row)"]!
                cell.imgCell.layer.cornerRadius = 1
                cell.imgCell.clipsToBounds = true
            }
        } else {
            //self.activityInd.stopAnimating()
            
            cell.lblCell.text = (datener[indexPath.row]).valueForKey("textname") as? String
            cell.lblCell.backgroundColor = UIColor(rgba: "#B20277BD")
            
            if (img_name["\(indexPath.row)"] != nil){
                
                cell.imgCell.image = img_name["\(indexPath.row)"]!
                cell.imgCell.layer.cornerRadius = 1
                cell.imgCell.clipsToBounds = true
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("Cell \(indexPath.row) selected")
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(self.COLLECTION_PADDING/2,self.COLLECTION_PADDING,self.COLLECTION_PADDING/2,self.COLLECTION_PADDING)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return self.COLLECTION_PADDING
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = self.view.frame.size.width/2 - (self.COLLECTION_PADDING * 2) + (self.COLLECTION_PADDING / 2)
        return CGSizeMake(size, size)
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return 153.0
        case 1, 2: return 160.0
        case 3: return 296.0
        case 4: return 73.0
        case 5:
            var numberOfRow: CGFloat = CGFloat(self.collectionView.numberOfItemsInSection(0))/2
            let f = numberOfRow - CGFloat(Int(numberOfRow))
            
            if f > 0 {
                numberOfRow = CGFloat(Int(numberOfRow + 1))
            }
            
            let height = numberOfRow * (self.view.frame.size.width/2 - (self.COLLECTION_PADDING * 2) + (self.COLLECTION_PADDING / 2)) + self.COLLECTION_PADDING * numberOfRow + self.COLLECTION_PADDING/2
            print("height = \(height) = \(numberOfRow) * \(self.view.frame.size.width/2 - (self.COLLECTION_PADDING * 2) + (self.COLLECTION_PADDING / 2)) + \(self.COLLECTION_PADDING * numberOfRow))")
            return height
        default: return 0
        }
    }
    
    //MARK: - Other
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        var body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendStringSmok("--\(boundary)\r\n")
                body.appendStringSmok("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendStringSmok("\(value)\r\n")
            }
        }
        
        let filename = "User-Profile.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendStringSmok("--\(boundary)\r\n")
        body.appendStringSmok("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendStringSmok("Content-Type: \(mimetype)\r\n\r\n")
        body.appendData(imageDataKey)
        body.appendStringSmok("\r\n")
        body.appendStringSmok("--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().UUIDString)"
    }
}

extension NSMutableData {
    func appendStringSmok(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}
