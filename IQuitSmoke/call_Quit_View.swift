//
//  call_Quit_View.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/2/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class call_Quit_View: UIViewController {
    
    @IBOutlet weak var bt_call: UIButton!
    @IBOutlet weak var image_View: UIImageView!
    @IBAction func ac_Calling(sender: UIButton) {
        
        UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:1600")!)
        sender.backgroundColor = UIColor.clearColor()
        //        var url:NSURL = NSURL(string: "tel://1600")!
        //        UIApplication.sharedApplication().openURL(url)
        println("Calling")
    }
    @IBAction func btnTouch(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        bt_call.layer.cornerRadius = bt_call.frame.size.width/2
        bt_call.layer.borderWidth = 2
        bt_call.layer.borderColor =  UIColor(rgba: "#0277BD").CGColor
        bt_call.clipsToBounds = true
        
        var imgListArray :NSMutableArray = []
        for countValue in 1...6
        {
            
            var strImageName : String = "quit_line_cover\(countValue)"
            var image  = UIImage(named:strImageName)
            imgListArray .addObject(image!)
        }
        
        self.image_View.animationImages = imgListArray as [AnyObject]
        self.image_View.animationDuration = 15
        self.image_View.startAnimating()
        self.image_View.layer.cornerRadius = 1
        self.image_View.clipsToBounds = true
     
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
}
