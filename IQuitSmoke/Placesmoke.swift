//
//  Placesmoke.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/10/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData


class Placesmoke: UITableViewController {
    
    @IBOutlet var activity_Indicaor: UIActivityIndicatorView!
    
    let pURL = "http://fahsai.bloodd.org/iquitsmoke/getAllPlaces.php"
    let pmainQueue = dispatch_get_main_queue()
    let pdiffQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

    var daten : NSMutableArray!{
        didSet{
            tableView.reloadData()
        }
    }
    
    var images = [String:UIImage?]()
    var title_name:String?
    var img_name = [String:UIImage?]()
    var dec_name:String?
    var datener = []
    
    var drawerController:KYDrawerController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "สถานที่ปลอดบุหรี่"
        
        if  isDatabaseEmpty() {
            loadJSON()
        }else{
            loadDatabase()
        }
        
//        var refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: Selector("sortArray"), forControlEvents: UIControlEvents.ValueChanged)
//        self.refreshControl = refreshControl
    }
    
    
//    func sortArray() {
//        loadJSON()
//        dispatch_async(self.pmainQueue){
//            self.tableView.reloadData()
//        }
//         refreshControl?.endRefreshing()
//    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//        if  isDatabaseEmpty() {
//            loadJSON()
//        }else{
//            loadDatabase()
//        }
//    }

    
    
    
    private func loadJSON(){
        
        let request = NSURLRequest(URL: NSURL(string: pURL)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            response, data, error in
            
            if (error != nil) {
                println(error!.localizedDescription)
                return
            }
            
            dispatch_async(self.pdiffQueue){
                // wird in anderer Queue ausfuhrt ->
                var error : NSError?
                
                if let data = (NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as! NSDictionary)["places"] as? NSMutableArray{
                    self.daten = data
                    self.saveDatabase()
                    
                }else{
                    if (error != nil) {
                        println(error?.localizedDescription)
                    }
                }
                
                dispatch_async(self.pmainQueue){
                    self.tableView.reloadData()
                }
            }
            
            self.activity_Indicaor.startAnimating()
            
        })
    }
    
    private func saveDatabase(){
        
        if self.daten != nil{
            
            for dic in self.daten{
                let url = dic["image"] as! String
                let data = NSData(contentsOfURL: NSURL(string: url)!)
                let image = UIImage(data: data!)
                self.images["\(self.daten.indexOfObject(dic))"] = image
                
                let _title = dic["title"] as! String
                
                let _description = dic["description"] as! String
                
                let _type = dic["type"] as! String
                
                let _startTime = dic["startTime"] as! String
                
                let _endTime = dic["endTime"] as! String
                
                let totalTime = ("\(_startTime) - \(_endTime) น.")
                
                
                //Add data Entities //File Model_data //Set AppDelegate
                
                var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                var context:NSManagedObjectContext = appDel.managedObjectContext!
                
                var newName = NSEntityDescription.insertNewObjectForEntityForName("Places", inManagedObjectContext: context) as! NSManagedObject
                
                newName.setValue(_title, forKey: "title")
                newName.setValue(image, forKey: "image")
                newName.setValue(_description, forKey: "text")
                newName.setValue(_type, forKey: "type_")
                newName.setValue(totalTime, forKey: "time")
                
                
                context.save(nil)
                
                println(newName)
                
            }
            self.activity_Indicaor.stopAnimating()
 
        }
        
    }
    
    
    private func isDatabaseEmpty() -> Bool{
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Places")
        fetch2.returnsObjectsAsFaults = false
        
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                
                println("11111Database is empty !!!")
                
                
            }else{
                
                let res = x[0]
                let url = res.valueForKey("image") as? String
                
                return false
            }
            
        }else{
            println("Not found anything")
        }
        
        return true
        
    }

    private func loadDatabase() -> Bool{
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Places")
        fetch2.returnsObjectsAsFaults = false
        if let x = context2.executeFetchRequest(fetch2, error: nil) as? [NSManagedObject]{
            if x.isEmpty{
                println("Database is empty !!!")
            }else{
                
                
                self.datener = context2.executeFetchRequest(fetch2, error: nil)!
                
                for dic in self.datener{
                    
                    var img1 = dic.valueForKey("image") as? UIImage
                    self.img_name["\(self.datener.indexOfObject(dic))"] = img1
                }
                
                return false
            }
            
        }else{
            println("Not found anything")
        }
        
        return true
        
    }
    
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if daten != nil{
            return daten.count
        }
        return datener.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 122
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! UITableViewCell
        
        
        if daten != nil{
            let _title = (daten[indexPath.row] as! NSDictionary)["title"] as? String
            let _description = (daten[indexPath.row] as! NSDictionary)["type"] as? String
            let _time1 = (daten[indexPath.row] as! NSDictionary)["startTime"] as? String
            let _time2 = (daten[indexPath.row] as! NSDictionary)["endTime"] as? String
            
            (cell.contentView.viewWithTag(40) as! UILabel).text = _title
            
            (cell.contentView.viewWithTag(50) as! UILabel).text = _description
            
            (cell.contentView.viewWithTag(60) as! UILabel).text = ("\(_time1!) - \(_time2!) น.")
            
            if (images["\(indexPath.row)"] != nil){
                
                (cell.contentView.viewWithTag(70) as! UIImageView).image = images["\(indexPath.row)"]!
                (cell.contentView.viewWithTag(70) as! UIImageView).layer.cornerRadius = 1
                (cell.contentView.viewWithTag(70) as! UIImageView).clipsToBounds = true
            }
        }
            
        else{
            let _title = (datener[indexPath.row]).valueForKey("title") as? String
            let _description = (datener[indexPath.row]).valueForKey("type_") as? String
            let _time = (datener[indexPath.row]).valueForKey("time") as? String
            
            
            (cell.contentView.viewWithTag(40) as! UILabel).text = _title
            
            (cell.contentView.viewWithTag(50) as! UILabel).text = _description
            
            (cell.contentView.viewWithTag(60) as! UILabel).text = _time
            
            if (img_name["\(indexPath.row)"] != nil){
                
            (cell.contentView.viewWithTag(70) as! UIImageView).image = img_name["\(indexPath.row)"]!
            (cell.contentView.viewWithTag(70) as! UIImageView).layer.cornerRadius = 1
            (cell.contentView.viewWithTag(70) as! UIImageView).clipsToBounds = true
                
            }
        }
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
   
        var title:String!
        var text:String!
        var calendar:String!
        var image:UIImage!
        
        if daten != nil{
            
            let _time1 = (daten[indexPath.row] as! NSDictionary)["startTime"] as? String
            let _time2 = (daten[indexPath.row] as! NSDictionary)["endTime"] as? String
            
            title = (daten[indexPath.row] as! NSDictionary)["title"] as? String
            text = (daten[indexPath.row] as! NSDictionary)["description"] as? String
            calendar = ("\(_time1!) - \(_time2!) น.")
            
            if (images["\(indexPath.row)"] != nil){  
                image = images["\(indexPath.row)"]!
            }
            
        }
        else{
            
            title = (datener[indexPath.row]).valueForKey("title") as? String
            text = (datener[indexPath.row]).valueForKey("text") as? String
            calendar = (datener[indexPath.row]).valueForKey("time") as? String
            
            if (img_name["\(indexPath.row)"] != nil){
                image = img_name["\(indexPath.row)"]!
            }
        }
        
        
        place.append(places(name_t: title!, text: text!, calendar: calendar!, image: image!))
        connextNavigation()
    }
    
    func connextNavigation(){
        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainPlace()
        }
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
}