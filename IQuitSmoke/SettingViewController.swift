//
//  SettingViewController.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/2/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData

class SettingViewController: UIViewController {
 
    //Edit UserName-------------------------------------------------
    @IBOutlet weak var view_UserName: UIView!
    @IBAction func ac_UserName(sender: UIButton) {
        self.view_UserName.backgroundColor = UIColor.clearColor()
    }
    @IBAction func btn_User_TouchDown(sender: UIButton) {
        self.view_UserName.backgroundColor = UIColor(rgba: "#400277BD")
    }
    // Check Box -----------------------------------------------------------------
    @IBOutlet weak var view_Check: Checkbox!
    @IBOutlet weak var view_Backgroud_Check: UIView!
    @IBAction func ac_check(sender: UIButton) {
        isCheck()
    }
    @IBAction func btn_Check_TouchDown(sender: UIButton) {
        self.view_Backgroud_Check.backgroundColor = UIColor(rgba: "#400277BD")
    }
    
    var num:String?
    var calender:String?
    var stateText : String?
    var isBool : Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_Check.layer.cornerRadius = 3.0
        view_Check.clipsToBounds = true
        
        view_Backgroud_Check.layer.borderWidth = 1.0
        view_Backgroud_Check.layer.borderColor = UIColor(rgba: "#EEEEEE").CGColor
        view_Backgroud_Check.clipsToBounds = true
        
        initChcek()
        isCheck()
        
    }
    
    func initChcek(){
        
        if num == "0"{
            isBool = true
        }else{
            isBool = false
        }
        
    }

    func isCheck(){
        
        if isBool == true {
            UIApplication.sharedApplication().clearKeepAliveTimeout()
            UIApplication.sharedApplication().cancelAllLocalNotifications()
            println("ปิด")
            isBool = false
            view_Check.isChecked = true
            num = "1"
        }else{
            UIApplication.sharedApplication().delegate?.description
            println("เปิด")
            view_Check.isChecked = false
            num = "0"
            isBool = true
        }
        self.view_Backgroud_Check.backgroundColor = UIColor.clearColor()
        view_Check.isChecked = !view_Check.isChecked
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
            
        }
    }
}
