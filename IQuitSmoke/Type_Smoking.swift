//
//  Type_Smoking.swift
//  ไทยไร้ควัน
//
//  Created by Apinat Jitrat on 11/12/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class Type_Smoking: UITableViewController {

    var dataSmokJson = loadJsonSmoking[loadJsonSmoking.count - 1]
    var textType_brand:String?
    var textType_price:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSmokJson.dataJsomSmok.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("typecell", forIndexPath: indexPath) as! typecellSmok
        
        let id = indexPath.row
        
        let brand = (dataSmokJson.dataJsomSmok[id]).valueForKey("brand") as? String
        let price = (dataSmokJson.dataJsomSmok[id]).valueForKey("price") as? String
        
        
        
        cell.lb_typeSmok.text = brand!
        cell.lb_prieceSmok.text = "\(price!) ฿"
        cell.targetForAction("getAction:", withSender: self)

        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let id = indexPath.row

        let brand = (dataSmokJson.dataJsomSmok[id]).valueForKey("brand") as? String
        let price = (dataSmokJson.dataJsomSmok[id]).valueForKey("price") as? String
        
        self.textType_brand = brand!
        self.textType_price = price!
        
        self.performSegueWithIdentifier("MainPersonal", sender: tableView)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MainPersonal" {
            let personal = segue.destinationViewController as! PersonalInformationTableViewController
            
            personal.type_brand = textType_brand
            personal.type_price = textType_price
        }
    }
}



class  typecellSmok: UITableViewCell {
    
    @IBOutlet var lb_typeSmok: UILabel!
    @IBOutlet var lb_prieceSmok: UILabel!

}

