//
//  TechniSmok.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/26/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit


class TechniSmok: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "เทคนิคเลิกบุหรี่"
        
        //Instantiation and the setting of the size and position
        let swiftPagesView : SwiftPages!
        swiftPagesView = SwiftPages(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
        
        //Initiation
        var VCIDs : [String] = ["Pages1","Pages2","Pages3","Pages4"]
        var buttonTitles : [String] = ["ล็อควัน", "ลั่นวาจา","พร้อมลงมือ","ลาอุปกรณ์"]
        
        swiftPagesView.enableBarShadow(true)
        swiftPagesView.setButtonsTextColor(UIColor.whiteColor())
        swiftPagesView.setAnimatedBarColor(UIColor.whiteColor())
        swiftPagesView.initializeWithVCIDsArrayAndButtonTitlesArray(VCIDs, buttonTitlesArray: buttonTitles) //ชื่อ
        swiftPagesView.setTopBarBackground(UIColor(rgba: "#0277BD"))
        swiftPagesView.setAnimatedBarColor(UIColor(red: 255/255, green: 250/255, blue: 205/255, alpha: 1.0))
        
        self.view.addSubview(swiftPagesView)
    }
   
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }

}


