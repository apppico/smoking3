//
//  LoginFBViewController.swift
//  IQiutSmoke_IOS
//
//  Created by Apinat Jitrat on 8/14/2558 BE.
//  Copyright (c) 2558 I'digimonasd. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Foundation
import CoreData

class LoginFBViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet var activityInd: UIActivityIndicatorView!
    
    var imagePic:String!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        //Function Facebook----------------------------------------------------------------------
    
        activityInd.hidden = true
        
        accessToken()
        
    }
   
    private func accessToken(){
        if (FBSDKAccessToken.currentAccessToken() == nil)
        {
        }
        else
        {
            println("Logged in...")
            var rq : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,name,email,gender", parameters: nil)
            rq.startWithCompletionHandler({ (connection, id user, err) -> Void in
                if (err == nil){
                    println("USER \(user)")
                    
                    var nameText = user["id"] as? String
                    var pic = "https://graph.facebook.com/\(nameText!)/picture?type=large"
                    println(pic)
                }
            })
        }
        
        var loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.center = self.view.center
        loginButton.delegate = self
        self.view.addSubview(loginButton)
    }
    
    
    
    
    // MARK: - Facebook Login----------------------------------------------------------------------
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        if error == nil
        {
            
            var rq : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,name,email,gender", parameters: nil)
            rq.startWithCompletionHandler({ (connection, id user, err) -> Void in
                if (err == nil){
                    println("USER \(user)")
                   
                    
                    var user_ID = user["id"] as? String
                    
                    var pic = "https://graph.facebook.com/\(user_ID!)/picture?type=large"
                    self.imagePic = pic
                    
                    println(pic)
                    
                    var email:NSString = user["email"] as! String
                    
                    var email2 = user["email"] as! String
                    
                    var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                    var context:NSManagedObjectContext = appDel.managedObjectContext!
                    let fetch = NSFetchRequest(entityName: "List")
                    fetch.returnsObjectsAsFaults = false
                    var results:NSArray = context.executeFetchRequest(fetch, error: nil)!
                    
                    if(results.count > 0){
                        
                        var res = results[results.count - 1] as! NSManagedObject
                        
                        var name = res.valueForKey("nameTx") as? String
                        var gender = res.valueForKey("genderTx") as? String
                        var age = res.valueForKey("ageTx") as? String
                        var disease = res.valueForKey("diseaseTx") as? String
     
    //check email data -------------------------------------------------------------------------------
                    
                    let post = "&referenceId=\(user_ID!)&userName=\(name!)&gender=\(gender!)&age=\(age!)&disease=\(disease!)"
                    
                    NSLog("PostData: %@",post);
                    
                    var url:NSURL = NSURL(string:"http://fahsai.bloodd.org/iquitsmoke/checkRegisterWithReferenceId.php")!
                    var postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                    
                    var postLength:NSString = String( postData.length )
                    
                    var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                    request.HTTPMethod = "POST"
                    request.HTTPBody = postData
                  
                    var reponseError: NSError?
                    var response: NSURLResponse?
                    
                    var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
                    
                    if ( urlData != nil ) {
                        let res = response as! NSHTTPURLResponse!;
                        
                        if (res.statusCode >= 200 && res.statusCode < 300)
                        {
                            var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                            
                            
                            println("GET\(responseData)")
                    
                            var error: NSError?
                           let jsonData = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary
        
                                if(err != nil) {
                                    // If there is an error parsing JSON, print it to the console
                                    println("JSON Error \(err!.localizedDescription)")
                                    
                                }
                            
                            if (jsonData != nil){
       //ถ้ามีข้อมูล อีเมล์ในดาต้าเบส จะทำการส่งไปยังหน้า main หลักของแอพพลิเคชัน----------------------------------
                                
                                    self.activityInd.hidden = false
                                    self.activityInd.startAnimating()
                                
                                    self.performSegueWithIdentifier("profile", sender: self)
                                
                                if let results: NSArray = jsonData!["Items"] as? NSArray {
                                    
                                    var _dictionary:NSDictionary
                                    for _dictionary in results {
                                        
                                        var _id = _dictionary["userId"] as! String
                                        var _name = _dictionary["userName"] as! String
                                        var _gender = _dictionary["gender"] as! String
                                        var _age = _dictionary["age"] as! String
                                        
                                        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                                        var context:NSManagedObjectContext = appDel.managedObjectContext!
                                        
                                        var newName = NSEntityDescription.insertNewObjectForEntityForName("Id", inManagedObjectContext: context) as! NSManagedObject
                                        newName.setValue(_id, forKey: "id")
                                        newName.setValue(_gender, forKey: "gender")
                                        newName.setValue(_name, forKey: "name")
                                        newName.setValue(_age, forKey: "age")
                                        
                                        if  let year = _dictionary["howLongSmoke"] as? String,
                                            let amount = _dictionary["smokeAmount"] as? String,
                                            let minute = _dictionary["smokeAfterWakeup"] as? String,
                                            let friends = _dictionary["friendSmoke"] as? String {
                                                
                                                var smokeName = NSEntityDescription.insertNewObjectForEntityForName("Smoking", inManagedObjectContext: context) as! NSManagedObject
                                                smokeName.setValue("" + year, forKey: "smok_year")
                                                smokeName.setValue("" + amount, forKey: "smok_roll")
                                                smokeName.setValue("" + minute, forKey: "smok_minute")
                                                smokeName.setValue("" + friends, forKey: "smok_person")
                                                
                                                context.save(nil)
                                                println(smokeName)
                                        }
                                        
                                        context.save(nil)
                                        println(newName)
                                        self.addJson()
                                        self.addHome()
                                        
                                    }
                                }
                            }
                        
    //ถ้าไม่มีข้อมูลในดาต้าเบส จะทำการส่งไปยังหน้า ขั้นตอนถัดไปป ---------------------------------------------------
                            else{
                                self.activityInd.hidden = false
                                self.activityInd.startAnimating()
                                
                                self.performSegueWithIdentifier("smoking", sender: self)
                                
                                var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                                var context:NSManagedObjectContext = appDel.managedObjectContext!
                                
                                var newName = NSEntityDescription.insertNewObjectForEntityForName("Facebook_id", inManagedObjectContext: context) as! NSManagedObject
                                newName.setValue(email2, forKey: "email_Fx")
                                newName.setValue(pic, forKey: "profile_Fx")
                                newName.setValue(user_ID, forKey: "id_facebook")
                                
                                context.save(nil)
                                println(newName)
                                self.addJson()
                                }
                                }
                            }
                        }
                    }
                })
        
        }  else
        {
            println(error.localizedDescription)
        }
        self.activityInd.hidden = true
        self.activityInd.startAnimating()
    }
    
    func addJson(){
        
        let data = NSData(contentsOfURL: NSURL(string: imagePic)!)
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newName = NSEntityDescription.insertNewObjectForEntityForName("Image", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue(data!, forKey: "imgProfile")
        println(newName)
        context.save(nil)
    }
    
    func addHome(){
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newName = NSEntityDescription.insertNewObjectForEntityForName("MainHome", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue("1", forKey: "nameMain")
        println(newName)
        context.save(nil)
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
    {
        println("User logged out...")
    }
}
