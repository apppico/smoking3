//
//  GenderViewController.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/14/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//
// Unused!
import UIKit
import Foundation
import CoreData
import MapKit
import CoreLocation

let kSavedItemsKey = "savedItems"


class Personal_information: UIViewController, UIActionSheetDelegate, UIAlertViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,UIPopoverPresentationControllerDelegate,MKMapViewDelegate, CLLocationManagerDelegate,AddGeotificationsViewControllerDelegate {
    
    @IBOutlet var scrolView: UIScrollView!
    
    @IBOutlet weak var textName: UITextField! = nil
    @IBAction func conNextAction(sender: AnyObject) {
        connextSaveData()
    }
    @IBOutlet weak var conNext_to: UIButton!
    @IBOutlet weak var textGender: UITextField!
    @IBAction func genderAction(sender: AnyObject) {
        genderAction()
    }
    @IBOutlet weak var textAge: UITextField!
    @IBAction func ageAction(sender: AnyObject) {
        ageAction()
    }
    @IBOutlet weak var diseaseText: UITextField!
    @IBAction func diseaseAction(sender: AnyObject) {
        diseaseAction()
    }
    @IBOutlet var lb_type: UILabel!
    @IBAction func typeSmokAction(sender: UIButton) {
//        typeSmok()
    }
    
    @IBOutlet weak var mapView: MKMapView!

    
    var rs : Array<NSDictionary>!
    var num : [Int] = []
    
    var type_brand:String?
    var type_price:String?
    
    var type_brand_1:String!
    var type_price_1:String!
    
    var geotifications = [Geotification]()
    let locationManager = CLLocationManager()
    var items = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrolView.contentSize.height = 970
        textName.delegate = self
        scrolView.delegate = self
    
        conNext_to.layer.cornerRadius = 3.0
        conNext_to.clipsToBounds = true
    
        
        // 1
        locationManager.delegate = self
        // 2
        locationManager.requestAlwaysAuthorization()
        // 3
        loadAllGeotifications()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        tapGesture.cancelsTouchesInView = false
        
        scrolView.addGestureRecognizer(tapGesture)
        
        setup()

    }
    
    func hideKeyboard() {
        textName.resignFirstResponder()
        var textName1 = textName.text
        addDataTextName.append(addDataTextFiledName(name: "\(textName1)"))
    }
    
    func setup(){
        loadData()
        loadDatainitClass()
        borderTextField()
        typeSmokk()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverSegue" {
            let popoverViewController = segue.destinationViewController as! UITableViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
            popoverViewController.popoverPresentationController!.delegate = self
        }
        
        if segue.identifier == "addGeotification" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let vc = navigationController.viewControllers.first as! AddGeotificationViewController
            vc.delegate = self
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    private func typeSmokk(){
        if(type_brand != nil){
            var name = count(type_brand!)
            lb_type.text = " \(type_brand!)  \(type_price!) ฿"
        }else{
            typeSmok()
            lb_type.text = " \(type_brand_1)  \(type_price_1) ฿"
        }
    }
    //เช็ค ข้อมูลตรง Text
    private func loadDatainitClass(){
        
            if(addDataTextName.count > 0){
                var res = addDataTextName[addDataTextName.count - 1]
                textName.text = res.name
            }else{
                textName.text = ""
            }
        
            if(addDataTextAge.count > 0){
                var res = addDataTextAge[addDataTextAge.count - 1]
                textAge.text = res.nameAge
            }else{
                textAge.text = " 17"
            }
    
            if(addDataTextDiease.count > 0){
                var res = addDataTextDiease[addDataTextDiease.count - 1]
                diseaseText.text = res.nameDiease
            }else{
                diseaseText.text = " ไม่มี"
            }
        
            if(addDataTextGender.count > 0){
                var res = addDataTextGender[addDataTextGender.count - 1]
                textGender.text = res.nameGender
            }else{
                textGender.text = " ชาย"
            }
    }
    
    func loadData(){
        
        let urlPath = "http://fahsai.bloodd.org/iquitsmoke/getDiseaseInfo.php"
        let url = NSURL(string: urlPath)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url!, completionHandler: {data, response, error -> Void in
            println("Task completed")
            if(error != nil) {
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            var err: NSError?
            if let jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary {
                if(err != nil) {
                    // If there is an error parsing JSON, print it to the console
                    println("JSON Error \(err!.localizedDescription)")
                }
                
                if let results: NSArray = jsonResult["Items"] as? NSArray {
                    // println("\(results)")
                    
                    self.rs = results as? Array<NSDictionary>
                }
            }
        })
        task.resume()
    }
    
    func genderAction(){
        let optionMenu = UIAlertController(title: "Gender", message: "กรุณาเลือกเพศ", preferredStyle: .ActionSheet)
        let deleteAction = UIAlertAction(title: "ชาย", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.textGender.text = " ชาย"
            addDataTextGender.append(addDataTextFiledGender(nameGender: " ชาย"))
        })
        let saveAction = UIAlertAction(title: "หญิง", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.textGender.text = " หญิง"
            addDataTextGender.append(addDataTextFiledGender(nameGender: " หญิง"))
        })
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
       // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    func ageAction(){
        let optionMenu = UIAlertController(title: "Age(อายุ)", message: "", preferredStyle: .ActionSheet)
        
            for i in 13...100{
                
                let disease = UIAlertAction(title: "\(i)", style: .Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.textAge.text = " \(i)"
                    addDataTextAge.append(addDataTextFiledAge(nameAge: " \(i)"))
                    
                })
                optionMenu.addAction(disease)
            }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func diseaseAction(){
        let optionMenu = UIAlertController(title: "โรค", message: "", preferredStyle: .ActionSheet)
        if rs != nil{
            for (id,str) in enumerate(rs){
                let str = rs[id]["diseaseName"] as! String
                
                let disease = UIAlertAction(title: "\(str)", style: .Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.diseaseText.text = " \(str)"
                    
                    addDataTextDiease.append(addDataTextFiledDiease(nameDiease: " \(str)"))
                })
                optionMenu.addAction(disease)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
 
    //load Data Smoking
    func typeSmok(){
        
        if (loadJsonSmoking.count != 0){
            
        var dataSmokJson = loadJsonSmoking[loadJsonSmoking.count - 1]
        
            let brand = (dataSmokJson.dataJsomSmok[0]).valueForKey("brand") as? String
            let price = (dataSmokJson.dataJsomSmok[0]).valueForKey("price") as? String
            
            self.type_brand_1 = "\(brand!)"
            self.type_price_1 = "\(price!)"
        }
    }
    

    
    
    
   //function ในการส่งค่าเข้า ดาต้าเบสเครื่อง และไว้สำหรับ กดไปยังหน้าต่อไปปป -----------------------------
   
    func connextSaveData(){
       var textName1:NSString = textName.text
        var textGender1:NSString = textGender.text
        var textAge1:NSString = textAge.text
        var textDisease1:NSString = diseaseText.text
        var textType:NSString = lb_type.text!
        
        //---
        if ( textName1.isEqualToString("") || textGender1.isEqualToString("") || textAge1.isEqualToString("") || textDisease1.isEqualToString("")) || textType.isEqualToString("") {
            
            var alertView:UIAlertView = UIAlertView()
            alertView.title = "กรอกข้อมูลไม่ครบ"
            alertView.message = "กรุณากรอกข้อมูลให้ครบทุกช่อง"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            
        } else {
            
            let apptoFacebook = NSBundle.mainBundle().bundleIdentifier
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(apptoFacebook!)
            self.performSegueWithIdentifier("goto_facebook", sender: self)
            
            //Add data Entities //File Model_data //Set AppDelegate
            
            var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            var context:NSManagedObjectContext = appDel.managedObjectContext!
            
            var newName = NSEntityDescription.insertNewObjectForEntityForName("List", inManagedObjectContext: context) as! NSManagedObject
            
            newName.setValue("" + textName.text, forKey: "nameTx")
            newName.setValue("" + textGender.text, forKey: "genderTx")
            newName.setValue("" + textAge.text, forKey: "ageTx")
            newName.setValue("" + diseaseText.text, forKey: "diseaseTx")
            
            context.save(nil)
            println(newName)
        }
    }
    
    // Animate TextField ขีดเส้นใต้ตรง Text
    
    func  borderTextField(){
        
        let width_ = CGFloat(2.0)
        
        let border_name = CALayer()
        border_name.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_name.frame = CGRect(x: 0, y: textName.frame.size.height - width_, width:  textName.frame.size.width, height: textName.frame.size.height)
        
        border_name.borderWidth = width_
        textName.layer.addSublayer(border_name)
        textName.layer.masksToBounds = true
        
        
        let border_gender = CALayer()
        border_gender.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_gender.frame = CGRect(x: 0, y: textGender.frame.size.height - width_, width:  textGender.frame.size.width, height: textGender.frame.size.height)
        
        border_gender.borderWidth = width_
        textGender.layer.addSublayer(border_gender)
        textGender.layer.masksToBounds = true
        
        
        let border_age = CALayer()
        border_age.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_age.frame = CGRect(x: 0, y: textAge.frame.size.height - width_, width:  textAge.frame.size.width, height: textAge.frame.size.height)
        
        border_age.borderWidth = width_
        textAge.layer.addSublayer(border_age)
        textAge.layer.masksToBounds = true
        
        let border_disease = CALayer()
        border_disease.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_disease.frame = CGRect(x: 0, y: diseaseText.frame.size.height - width_, width:  diseaseText.frame.size.width, height: diseaseText.frame.size.height)
        
        border_disease.borderWidth = width_
        diseaseText.layer.addSublayer(border_disease)
        diseaseText.layer.masksToBounds = true
        
        let border_typeSmok = CALayer()
        border_typeSmok.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_typeSmok.frame = CGRect(x: 0, y: lb_type.frame.size.height - width_, width:  lb_type.frame.size.width, height: lb_type.frame.size.height)
        
        border_typeSmok.borderWidth = width_
        lb_type.layer.addSublayer(border_typeSmok)
        lb_type.layer.masksToBounds = true
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: Loading and saving functions
    
    func loadAllGeotifications() {
        
        if let savedItems = NSUserDefaults.standardUserDefaults().arrayForKey(kSavedItemsKey) {
            for savedItem in savedItems {
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification {
                    
                    let item = NSKeyedArchiver.archivedDataWithRootObject(geotification)
                    
                    items.addObject(item)
                    
                }
            }
        }
        getMapView()
    }
    
    func getMapView(){
        
        var  geotification:Geotification?
        
        if(items.count > 0){
            
            var nee:AnyObject = items[items.count - 1]
            
            geotification = NSKeyedUnarchiver.unarchiveObjectWithData(nee as! NSData) as? Geotification
            
            addGeotification(geotification!)
            
        }else{
            
        }
        if(geotification != nil){
            var latitude = geotification?.coordinate.latitude
            var longitude = geotification?.coordinate.longitude
            var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1)
            var mylocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!,longitude!)
            var myRegion:MKCoordinateRegion = MKCoordinateRegionMake(mylocation, theSpan)
            self.mapView.setRegion(myRegion, animated: true)
        }else{
            
        }
        
    }
    
    func saveAllGeotifications() {
        var items = NSMutableArray()
        
        for (id,geotification) in enumerate(geotifications) {
            let item = NSKeyedArchiver.archivedDataWithRootObject(geotification)
            
            items.addObject(item)
            
            removeGeotification(geotification)
            saveAllGeotifications()
        }
        
        NSUserDefaults.standardUserDefaults().setObject(items, forKey: kSavedItemsKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    // MARK: Functions that update the model/associated views with geotification changes
    
    func addGeotification(geotification: Geotification) {
        geotifications.append(geotification)
        mapView.addAnnotation(geotification)
        addRadiusOverlayForGeotification(geotification)
    }
    
    func removeGeotification(geotification: Geotification) {
        if let indexInArray = find(geotifications, geotification) {
            geotifications.removeAtIndex(indexInArray)
        }
        
        mapView.removeAnnotation(geotification)
        removeRadiusOverlayForGeotification(geotification)
    }
    
    // MARK: AddGeotificationViewControllerDelegate
    
    func addGeotificationViewController(controller: AddGeotificationViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        // 1
        let clampedRadius = (radius > locationManager.maximumRegionMonitoringDistance) ? locationManager.maximumRegionMonitoringDistance : radius
        
        var geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note)
        
        geotifications.append(geotification)
        // 2
        startMonitoringGeotification(geotification)
        
        saveAllGeotifications()
        
        loadAllGeotifications()
        
    }
    
    // MARK: MKMapViewDelegate
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        let identifier = "myGeotification"
        if annotation is Geotification {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                var removeButton = UIButton.buttonWithType(.Custom) as! UIButton
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteGeotification")!, forState: .Normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = UIColor.purpleColor()
            circleRenderer.fillColor = UIColor.purpleColor().colorWithAlphaComponent(0.4)
            return circleRenderer
        }
        return nil
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        // Delete geotification
        var geotification = view.annotation as! Geotification
        stopMonitoringGeotification(geotification)
        removeGeotification(geotification)
        saveAllGeotifications()
    }
    
    // MARK: Map overlay functions
    
    func addRadiusOverlayForGeotification(geotification: Geotification) {
        mapView?.addOverlay(MKCircle(centerCoordinate: geotification.coordinate, radius: geotification.radius))
    }
    
    func removeRadiusOverlayForGeotification(geotification: Geotification) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        if let overlays = mapView?.overlays {
            for overlay in overlays {
                if let circleOverlay = overlay as? MKCircle {
                    var coord = circleOverlay.coordinate
                    if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                        mapView?.removeOverlay(circleOverlay)
                        break
                    }
                }
            }
        }
    }
    
    // MARK: Other mapview functions
    
    @IBAction func zoomToCurrentLocation(sender: AnyObject) {
        zoomToUserLocationInMapView(mapView)
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .AuthorizedAlways)
    }
    
    func regionWithGeotification(geotification: Geotification) -> CLCircularRegion {
        // 1
        let region = CLCircularRegion(center: geotification.coordinate, radius: 0, identifier: geotification.identifier)
        // 2
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func startMonitoringGeotification(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
            showSimpleAlertWithTitle("Error", message: "Geofencing is not supported on this device!", viewController: self)
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
            showSimpleAlertWithTitle("Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.", viewController: self)
        }
        // 3
        let region = regionWithGeotification(geotification)
        // 4
        locationManager.startMonitoringForRegion(region)
    }
    
    func stopMonitoringGeotification(geotification: Geotification) {
        for region in locationManager.monitoredRegions {
            if let circularRegion = region as? CLCircularRegion {
                if circularRegion.identifier == geotification.identifier {
                    locationManager.stopMonitoringForRegion(circularRegion)
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, monitoringDidFailForRegion region: CLRegion!, withError error: NSError!) {
        println("Monitoring failed for region with identifier: \(region.identifier)")
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Location Manager failed with the following error: \(error)")
    }
    
}


