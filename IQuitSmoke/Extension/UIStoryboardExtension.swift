//
//  UIStoryboardExtension.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/15/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

extension UIStoryboard{
    
    class func mainStoryboard() -> UIStoryboard
    {
        return UIStoryboard (name: "Main-6", bundle: NSBundle.mainBundle())
    }
    
    class  func  mainNavigation()  -> UINavigationController {
        return   mainStoryboard().instantiateViewControllerWithIdentifier("MainNavigation") as! UINavigationController
    }
    class func mainTechniSmok() -> UINavigationController{
        return mainStoryboard().instantiateViewControllerWithIdentifier("TechniSmok") as! UINavigationController
    }
    
    class  func  mainCalendar()  -> UINavigationController {
        return   mainStoryboard().instantiateViewControllerWithIdentifier("calendar") as! UINavigationController
    }
    
    class func mainShare() -> UINavigationController {
        return mainStoryboard().instantiateViewControllerWithIdentifier("share") as! UINavigationController
    }
    class  func  mainCalling()  -> UINavigationController {
        return   mainStoryboard().instantiateViewControllerWithIdentifier("call_Quit") as! UINavigationController
    }
  
    class  func mainLa_device() -> UINavigationController {
        return  mainStoryboard().instantiateViewControllerWithIdentifier("la_device") as! UINavigationController
    }
    
    class  func  mainSetting()  -> UINavigationController {
        return   mainStoryboard().instantiateViewControllerWithIdentifier("setting") as! UINavigationController
    }
    class func mainClinic() -> UINavigationController {
        return mainStoryboard().instantiateViewControllerWithIdentifier("clinic") as! UINavigationController
    }
    class func mainStaff() -> UINavigationController {
        return mainStoryboard().instantiateViewControllerWithIdentifier("Staff") as! UINavigationController
    }
    class func mainTabViewPages() -> UINavigationController {
        return  mainStoryboard().instantiateViewControllerWithIdentifier("TabbarView") as! UINavigationController
    }
    
    class func mainPlace() -> UINavigationController {
        return  mainStoryboard().instantiateViewControllerWithIdentifier("placess") as! UINavigationController
    }
    
    class  func  mainRecomment()  -> UINavigationController {
        return   mainStoryboard().instantiateViewControllerWithIdentifier("Recomment") as! UINavigationController
    }
    
    
}
