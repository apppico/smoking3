//
//  ClinicTableViewController.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/16/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreLocation

class ClinicTableViewController: UITableViewController,  CLLocationManagerDelegate {
    
    
    let locationManager = CLLocationManager()
    var latlong:String!
    let mainQueue = dispatch_get_main_queue()
    let diffQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    var daten : NSMutableArray!
    var daten2 : NSMutableArray!
    var namePlace:[String]!
    var nameStreet:String?
    
    var province:String!
    var datener = []
    let mainClinic = dispatch_get_main_queue()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        var refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: Selector("sortArray"), forControlEvents: UIControlEvents.ValueChanged)
        self.refreshControl = refreshControl
        sortArray()
        
    }
    
    
    func sortArray() {
      
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        dispatch_async(self.mainClinic){
            self.tableView.reloadData()
        }
          refreshControl?.endRefreshing()
    }
    
    func getDataProvince(){
        
        let urlPath: String = "http://fahsai.bloodd.org/getServiceCenterByProvince.php"
        var url: NSURL = NSURL(string: urlPath)!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        var stringPost="province=\(province)" // Key and Value จังหวัด
        
        let data = stringPost.dataUsingEncoding(NSUTF8StringEncoding)
        
        request.timeoutInterval = 60
        request.HTTPBody=data
        request.HTTPShouldHandleCookies=false
        
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (jsonResult != nil) {
                
                if let results: NSArray = jsonResult["Items"] as? NSArray {
          
                    self.datener = results
                    
                        dispatch_async(self.mainClinic){
                            self.tableView.reloadData()
                        }
                }
            }else {
                // Failed
                println("Failed")
            }
            
        })
        
    }
    
    
    func getDataDistrict(){
        
        let urlPath: String = "http://fahsai.bloodd.org/getServiceCenterByDistrict.php"
        var url: NSURL = NSURL(string: urlPath)!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        var stringPost="district=\(province)"// Key and Value เขต
        
        let data = stringPost.dataUsingEncoding(NSUTF8StringEncoding)
        
        request.timeoutInterval = 60
        request.HTTPBody=data
        request.HTTPShouldHandleCookies=false
        
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (jsonResult != nil) {
                if let results: NSArray = jsonResult["Items"] as? NSArray {
                    
//                    println("\(results)")
                    self.datener = results
                    
                    dispatch_async(self.mainQueue){
                        self.tableView.reloadData()
                    }
                }
            }else {
                // Failed
                println("Failed")
            }
            
        })
        
    }
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datener.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cellClinic", forIndexPath: indexPath) as! ClinicCell
        
        let name = (datener[indexPath.row]).valueForKey("name") as? String
        let tel = (datener[indexPath.row]).valueForKey("tel") as? String
        
        cell.lb_nameClinic.text  =  name!
        cell.btn_cellClinic.addTarget(self, action: "cellingClinic:", forControlEvents: .TouchUpInside)
        cell.btn_cellClinic.addTarget(self, action: "cellingDown:", forControlEvents: .TouchDown)
        cell.btn_cellClinic.tag = indexPath.row

        cell.btn_cellClinic.layer.cornerRadius = cell.btn_cellClinic.frame.size.width/2
        cell.btn_cellClinic.layer.borderWidth = 1.5
        cell.btn_cellClinic.layer.borderColor =  UIColor(rgba: "#0277BD").CGColor
        cell.btn_cellClinic.clipsToBounds = true

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let mycell  = self.storyboard?.instantiateViewControllerWithIdentifier("ClinicData") as! ClinicDataView
        self.navigationController?.pushViewController(mycell, animated: true)

        mycell.str_Name = (datener[indexPath.row]).valueForKey("name") as? String
        mycell.str_Place = (datener[indexPath.row]).valueForKey("address") as? String
        mycell.str_Tel = (datener[indexPath.row]).valueForKey("tel") as? String
        mycell.str_Map = (datener[indexPath.row]).valueForKey("latlng") as? String
    }
   
    @IBAction func cellingClinic(sender: UIButton) {
        
        sender.backgroundColor = UIColor.clearColor()
        
        let titleString = (datener[sender.tag]).valueForKey("tel") as? String
        if (titleString != nil){
            
            let celling = titleString!.componentsSeparatedByString(",")
            let celling2 = celling[0]
            let cellingClinic = celling2.componentsSeparatedByString("-")
            var str:String = ""
            
            for var i = 0 ; i < cellingClinic.count; i++  {
                str += cellingClinic[i]
            }
            
            var cell = str.componentsSeparatedByString(" ")
            UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(cell[0])")!)
            println("\(cell[0])")
        
        }else{
            
        }
    }
    
    @IBAction func cellingDown(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }

    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
            
        }
    }

    func loadJSON(){
        let urlPath = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(latlong)&language=th"
        let request = NSURLRequest(URL: NSURL(string: urlPath)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            response, data, error in
            
            if (error != nil) {
                println(error!.localizedDescription)
                return
            }
            dispatch_async(self.diffQueue){
                // wird in anderer Queue ausfuhrt ->
                var error : NSError?
                if let data = (NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as! NSDictionary)["results"] as? NSMutableArray{
                    self.daten = data
                }else{
                    if (error != nil) {
                        println(error?.localizedDescription)
                    }
                }
                dispatch_async(self.mainQueue){
                    self.getLocation()
                }
            }
            //            self.activityInd.startAnimating()
        })
        
        
    }
    
    func getLocation(){
        if self.daten != nil{
            if let url = (daten[0] as! NSDictionary)["address_components"] as? NSMutableArray{
                self.daten2 = url
            }else{
                
            }
            if let url2 = (daten[0] as! NSDictionary)["types"] as? NSMutableArray {
                
                self.nameStreet  = String(url2[0] as! NSString)
                println("\(self.nameStreet!)")
            }
            
        }
        place()
    }
    
    func place(){
        
        if daten2 != nil {
            if (self.nameStreet == "street_address"){
                let _title = (daten2[4] as! NSDictionary)["short_name"] as? String
                if (_title == "กรุงเทพมหานคร"){
                    var nameDic = (daten2[3] as! NSDictionary)["short_name"] as? String
                    var strNamePlace = nameDic!.componentsSeparatedByString("เขต ")
                    self.province = strNamePlace[1]
                    getDataDistrict()
                 }else{
                var strNamePlace = _title!.componentsSeparatedByString("จ.")
                self.province = strNamePlace[1]
                getDataProvince()
                }
                
            }else{
                let _title = (daten2[3] as! NSDictionary)["short_name"] as? String
                if (_title == "กรุงเทพมหานคร"){
                    var nameDic = (daten2[2] as! NSDictionary)["short_name"] as? String
                    var strNamePlace = nameDic!.componentsSeparatedByString("เขต ")
                    self.province = strNamePlace[1]
                    getDataDistrict()
                }else{
                    var strNamePlace = _title!.componentsSeparatedByString("จ.")
                    self.province = strNamePlace[1]
                    getDataProvince()
                }
                
            }

        }
    }
}

extension ClinicTableViewController : CLLocationManagerDelegate{
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            
            var currentLocation : CLLocation = locations[0] as! CLLocation
            
            if (error != nil) {
                println("Error: " + error.localizedDescription)
                return
            }
            if placemarks.count > 0 {
                //                let pm = placemarks[placemarks.count-1] as! CLPlacemark
                var stringLongitude : NSString = NSString(format: "%0.6f", currentLocation.coordinate.longitude)
                var stringLatitude : NSString = NSString(format: "%0.6f", currentLocation.coordinate.latitude)
                
                self.latlong = "\(stringLatitude),\(stringLongitude)"
                self.loadJSON()
                
            } else {
                println("Error with the data.")
            }
            self.locationManager.stopUpdatingLocation()
        })
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error: " + error.localizedDescription)
    }
    
}


class  ClinicCell: UITableViewCell {
    @IBOutlet var lb_nameClinic: UILabel!
    @IBOutlet var btn_cellClinic: UIButton!
    
}