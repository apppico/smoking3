//
//  ViewController.swift
//  IQuitSmoke
//
//  Created by iAmMac on 8/13/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import MapKit


class ViewController: UIViewController,UIScrollViewDelegate,CLLocationManagerDelegate {
        
        @IBOutlet var scrollView: UIScrollView!
        @IBOutlet var textView: UITextView!
    
        @IBOutlet var img_View2: UIImageView!
        @IBOutlet var startButton: UIButton!
    
        let locationManager = CLLocationManager()

    
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view, typically from a nib.
            
            //1
            self.scrollView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
            let scrollViewWidth:CGFloat = self.scrollView.frame.width
            let scrollViewHeight:CGFloat = self.scrollView.frame.height
            //2
            textView.textAlignment = .Center
            textView.textColor = .blackColor()
            self.startButton.layer.cornerRadius = 3.0
            
            textView.text = "ห่างไกลจากภัยร้ายบุหรี่ \n เลิกบุหรี่ตั้งแต่วันนี้เพื่อตนเองและคนใกล้ชิด \n\n\n แอพ I Quit Smoke \nจะช่วยเลิกบุหรี่โดยเปลี่ยนการเลิกให้เป็นง่าย"
            textView.font = UIFont(name:"Avenir", size:22)
            img_View2.image = UIImage(named: "Icon2")
            
            self.locationManager.requestWhenInUseAuthorization()
            
            
            // Show the "Let's Start" button in the last slide (with a fade in animation)
        }
 
}