//
//  calendar_day.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/24/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class calendar_day: UIViewController {
    
    //function ในการเลือกข้อมูลวันที่ --------------------------------------------------------------------
    @IBOutlet weak var tx_Pic: UITextField!
    @IBAction func picKer_day(sender: AnyObject) {
        calendarBtn()
    }
    @IBAction func calendar(sender: AnyObject) {
        connextSaveData()
    }
    @IBOutlet weak var next_smok: UIButton!
    
    
    var _dayTotal : Int? = 0
    var allWeek : Int?
    var allDay : Int?
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
        next_smok.layer.cornerRadius = 3.0
        next_smok.clipsToBounds = true
        
        let date1 = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        formatter.stringFromDate(date1)
        let dateFormat1  = NSDateFormatter()
        dateFormat1.dateFormat = "d/M/yyyy"
        
        let datatwo = dateFormat1.stringFromDate(date1)
        
        self.tx_Pic.text = datatwo
    
       let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeekString = dateFormatter.stringFromDate(date1)
        println("วันนี้ \(dayOfWeekString)")
        
        func printTimestamp() {
            let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
            println(timestamp)
        }
            printTimestamp()
        // Do any additional setup after loading the view.
    }
    func getDayOfWeek(today:String)->Int {
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "d/M/yyyy"
        let todayDate = formatter.dateFromString(today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
        let dayOfWeek = (myComponents.weekday - myCalendar.firstWeekday)
        
        return dayOfWeek
    }
    
    func calendarBtn(){
        
        DatePickerDialog().show(title: "ปฏิทิน", doneButtonTitle: "ตกลง", cancelButtonTitle: "ยกเลิก", datePickerMode: UIDatePickerMode.DateAndTime) {
            (date) -> Void in
            
            let dateFormat : NSDateFormatter = NSDateFormatter()
            dateFormat.dateFormat = "d/M/yyyy"
            
            let dataend = dateFormat.stringFromDate(date)
            
            let date_ = NSDateFormatter()
            date_.dateFormat = "EEEE"
            let dayOfWeekString = date_.stringFromDate(date)
            println("วันสุดท้าย \(dayOfWeekString)")
            
            //function วันที่ปัจจุบัน----------------------------------------
            let date1 = NSDate()
            let formatter = NSDateFormatter()
            formatter.timeStyle = .ShortStyle
            formatter.stringFromDate(date1)
            let dateFormat1 : NSDateFormatter = NSDateFormatter()
            dateFormat1.dateFormat = "d/M/yyyy"
            
            let datastart = dateFormat1.stringFromDate(date1)
          //เงื่อนไข---------------------------------------------------
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "d/M/yyyy"
            
            let startDate:NSDate = dateFormatter.dateFromString(datastart)!
            let endDate:NSDate = dateFormatter.dateFromString(dataend)!
            
            let cal = NSCalendar.currentCalendar()
            
            
            let unit:NSCalendarUnit = .CalendarUnitDay
            
            // fuction นับจำนวนวัน
            let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: nil)
            
            
            let day = components.day + 1
            let day1 = "\(day) วัน"
            
            
            self.tx_Pic.text = "\(dataend)"
            
            self._dayTotal = day
            // function นับจำนวน ว่าวันที่เริ่ม เป็นสัปดาห์ที่เท่าไหร่ของปี
            let calendar = NSCalendar.currentCalendar();
            calendar.firstWeekday = 2;
            
            
            var weekNum_Start = calendar.components(NSCalendarUnit.CalendarUnitWeekOfYear, fromDate: startDate).weekOfYear;
            var weekNum_End = calendar.components(NSCalendarUnit.CalendarUnitWeekOfYear, fromDate: endDate).weekOfYear;
            
            println("weekStart\(weekNum_Start) weekEnd\(weekNum_End)")
            
            //-------------------------
            let weekday_start = self.getDayOfWeek(datastart)
            println("วันที่\(weekday_start)ของสัปดาห์")
            
            let weekday_end = self.getDayOfWeek(dataend)
            println("วันที่\(weekday_end)ของสัปดาห์")
            
            // วันเริ่มต้นของสัปดาห์ ----------------------------------------------------
            
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let currentDateComponents = myCalendar.components(.CalendarUnitYearForWeekOfYear | .CalendarUnitWeekOfYear , fromDate: startDate)
            
            let startOfWeek = cal.dateFromComponents(currentDateComponents)
            
            let dateFormat_week_start : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start.dateFormat = "d"
            let date_st_d = dateFormat_week_start.stringFromDate(startOfWeek!)
            
            let dateFormat_week_start1 : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start1.dateFormat = "M"
            let date_st_m = dateFormat_week_start1.stringFromDate(startOfWeek!)
            
            let dateFormat_week_start2 : NSDateFormatter = NSDateFormatter()
            dateFormat_week_start2.dateFormat = "yyyy"
            let date_st_y = dateFormat_week_start2.stringFromDate(startOfWeek!)
            
            var Day:Int! = date_st_d.toInt()
            var Moth:Int! = date_st_m.toInt()
            var Year:Int! = date_st_y.toInt()
            
            if(weekNum_End >= weekNum_Start){
                self.allWeek = (weekNum_End - weekNum_Start) + 1
            }else{
                self.allWeek = (52 - (weekNum_Start - weekNum_End)) + 1
            }
            
            self.allDay = self.allWeek! * 7
            
            println("\(self.allDay!) วันทั้งหมด")
            
            var str : String = ""
            var inprogram : String = ""
            
            
            for(var i=1; i <= self.allDay; i++){
                let dateCom_ = NSDateComponents()
                
                dateCom_.day = Day + i
                dateCom_.month = Moth
                dateCom_.year = Year
                
                var someDate: NSDate = NSCalendar.currentCalendar().dateFromComponents(dateCom_)!
                let dateForma_ = NSDateFormatter()
                dateForma_.dateFormat = "d/M/yyyy"
                let total_ = dateForma_.stringFromDate(someDate)
                
                if ((startDate.compare(someDate) == NSComparisonResult.OrderedAscending) || (startDate.compare(someDate) == NSComparisonResult.OrderedSame)) && ((endDate.compare(someDate) == NSComparisonResult.OrderedDescending) || (endDate.compare(someDate) == NSComparisonResult.OrderedSame)){
                    
                    inprogram += "\(1),"
                }else{
                    inprogram += "\(0),"
                }
                if i != self.allDay{
                    str += "\(total_),"
                }else{
                    str += "\(total_)"
                }
            }
            println(str)
            println(inprogram)
            // upload เข้า data เครื่อง
            var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            var context:NSManagedObjectContext = appDel.managedObjectContext!
            
            var newName = NSEntityDescription.insertNewObjectForEntityForName("Calendar", inManagedObjectContext: context) as! NSManagedObject
            newName.setValue("" + day1, forKey: "id_Day")
            newName.setValue("" + datastart, forKey: "start_Day")
            newName.setValue("" + dataend, forKey: "end_Day")
            newName.setValue("" + str, forKey: "total_Day")
            newName.setValue("" + inprogram, forKey: "inprogram")
            
            context.save(nil)
            println(newName)
        }
    }
    
    func connextSaveData(){
        
        if _dayTotal == 0 {
            
            var alertView:UIAlertView = UIAlertView()
            alertView.title = "กรุณาเลือกวันที่ต้องการเลิกบุหรี่"
            alertView.message = "คุณยังไม่ได้เลือกวันที่ต้องการเลิก"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            
        }else{
            self.performSegueWithIdentifier("smok_gotoMain", sender: self)
        }
    }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
