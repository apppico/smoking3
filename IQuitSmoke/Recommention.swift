
//
//  Recommention.swift
//  CustomUITableViewCell
//
//  Created by Apinat Jitrat on 9/9/2558 BE.
//  Copyright (c) 2558 Knowstack. All rights reserved.
//

import UIKit

class Recommention: UIViewController {
    
    @IBOutlet weak var view_Text: UIView!
    @IBOutlet weak var img_Pic: UIImageView!
    @IBOutlet weak var lb_Title: UILabel!
    @IBOutlet weak var tx_Des: UITextView!
    @IBAction func btn_TabberView(sender: UIBarButtonItem) {
        let mycell  = self.storyboard?.instantiateViewControllerWithIdentifier("tabberview") as! TabbarViewPage
        self.navigationController?.pushViewController(mycell, animated: true)
    }
    
    @IBAction func btn_ActionQuitLine(sender: UIButton) {
        connextQuieline()
    }
    @IBOutlet var btn_QuitLine: UIButton!
    
    @IBAction func btn_ActionClinic(sender: UIButton) {
        connextClinic()
    }
    @IBOutlet var btn_Clinic: UIButton!
    
    var description_:String?
    var image: UIImage?
    var title_:String?
    var drawerController:KYDrawerController?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_Text.layer.shadowOpacity = 0.2
        view_Text.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_Text.layer.shadowRadius = 15.0
        view_Text.layer.shadowColor = UIColor.blackColor().CGColor
        
        lb_Title.layer.shadowColor = UIColor.blackColor().CGColor
        lb_Title.layer.shadowOffset = CGSizeMake(10, 10)
        lb_Title.layer.shadowRadius = 15
        lb_Title.layer.shadowOpacity = 1

        
        cuttomview()
        
        img_Pic.layer.cornerRadius = 1
        img_Pic.clipsToBounds = true
        
        
     
        // Do any additional setup after loading the view.
    }
    
    func cuttomview(){
        
        if(recommen.count > 0){
            var res = recommen[recommen.count - 1]
            
            title = res.title
            lb_Title.text = res.title
            tx_Des.text = res.dec
            img_Pic.image = res.image
            
            if(res.title == "หาที่ปรึกษา"){
                // Show the "Let's Start" button in the last slide (with a fade in animation)
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.btn_QuitLine.alpha = 2.0
                    self.btn_Clinic.alpha = 2.0
                })
            }else{
                
            }
            
        }else{
            println("")
        }
        
    }
    
    func connextClinic(){
        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainClinic()
        }
        
    }
    
    func connextQuieline(){
        if(dra.count > 0){
            var res = dra[dra.count - 1]
            drawerController = res.drawerController
        }else{
            println("")
        }
        
        if (drawerController != nil) {
            drawerController!.mainViewController = UIStoryboard.mainCalling()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}



