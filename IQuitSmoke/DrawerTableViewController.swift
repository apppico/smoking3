/*
Copyright (c) 2015 Kyohei Yamaguchi. All rights reserved.

*/

import UIKit
import Foundation
import CoreData
import QuartzCore

class DrawerTableViewController: UITableViewController {

    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    

    var isCheck:Bool!
    var menuSection : [[(level : Int, hidden : Bool, hasDetail : Bool , title : String , image : String, viewController : UINavigationController!)]]!
    
    var presentIndex = [[Int]]()
    let mmainQueue = dispatch_get_main_queue()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMenuData()
        updatePresentIndex()
        dispatch_async(self.mmainQueue){
            self.loadViewName()

        }
        
        let drawerController = navigationController?.parentViewController as? KYDrawerController
        
        dra.append(tabbarController(drawerController: drawerController!))
        
        
        //คำสั่งในการส่งค่า ของแต่ละ function
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"showTest:", name: "actionTwoPressed", object: nil)
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"showMessage:", name: "actionOnePressed", object: nil)
    }

    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        
        img_profile.layer.cornerRadius = img_profile.frame.size.width/2
        img_profile.layer.masksToBounds = true

        dispatch_async(self.mmainQueue){
            self.loadViewName()
        }
        loadImage()
    }
    
    
    func showMessage(notification:NSNotification){
        var message:UIAlertController = UIAlertController(title: "ยินดีต้อนรับเข้าสู่แอพพลิเคชัน", message: "สวัสดี :)", preferredStyle: UIAlertControllerStyle.Alert)
        message.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(message, animated: true, completion: nil)
        
    }
    
    func showTest(notification:NSNotification){
        var message:UIAlertController = UIAlertController(title: "ยินดีต้อนรับเข้าสู่แอพพลิเคชัน", message: "สวัสดี :)", preferredStyle: UIAlertControllerStyle.Alert)
        message.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(message, animated: true, completion: nil)
        
        println("bedsssssssss")
        
    }
    
    
    func loadViewName(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Id")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            var _username = res.valueForKey("name") as? String
            self.lb_name.text = _username!
            
        }else{
            println("errorID_Name")
            
        }
       
    }
    
    func loadImage(){
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Image")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            var _userimage = res.valueForKey("imgProfile") as? NSData
            
            if _userimage != nil{
                
                let image = UIImage(data: _userimage!)
                self.img_profile.image = image
                img_profile.layer.cornerRadius = img_profile.frame.size.width/2
                img_profile.layer.masksToBounds = true
                
            }else{
                println("Error Image")
            }
            
        }else{
            println("NODATAJSON")
            
        }
        
    }
    
    private func initMenuData(){
        
        menuSection = [
            [
                (level : 0 ,hidden : false, hasDetail : true , title : "ข้อมูลส่วนตัว", image : "ic_risky", viewController : UIStoryboard.mainNavigation()),
                
                (level : 0,hidden : false, hasDetail : true , title : "เทคนิคเลิกบุหรี่" ,image : "ic_cigarette",viewController : UIStoryboard.mainTechniSmok()),
                
                (level : 1,hidden : false, hasDetail : true , title : "ล๊อควัน" ,image : "ic_account_child",
                    viewController : UIStoryboard.mainCalendar()),
                
                (level : 1,hidden : false, hasDetail :true,title : "ลั่นวาจา",image : "ic_tell",
                    viewController : UIStoryboard.mainShare()),
                
                (level : 1,hidden : false, hasDetail : true , title : "พร้อมลงมือ",image : "ic_loyalty",
                    viewController : UIStoryboard.mainTabViewPages()),
               
                (level : 1,hidden : false, hasDetail :true,title : "ลาอุปกรณ์",image : "ic_quitline",viewController : UIStoryboard.mainLa_device()),
                
                (level : 0,hidden : false, hasDetail : true, title:"QuitLine",image :"ic_quick_contacts",
                    viewController : UIStoryboard.mainCalling()),
                
                (level : 0,hidden : false, hasDetail : true , title : "คลินิกฟ้าใส",image : "clinic",
                    viewController : UIStoryboard.mainClinic()),
                
                (level : 0,hidden : false, hasDetail : true , title : "คณะผู้จัดทำ",image : "ic_contributor",
                    viewController : UIStoryboard.mainStaff())
            ],
            [
                (level : 0,hidden : false, hasDetail : true , title : "ตั้งค่า",image : "ic_settings",
                    viewController : UIStoryboard.mainSetting()),
                
                (level : 0,hidden : false, hasDetail : true , title : "ออกจากระบบ",image : "ic_lockout",
                    viewController : nil)
            ]
        ]
    }
   
    private func updatePresentIndex(){
        presentIndex = [[]]
        for (section,sectionData) in enumerate(menuSection){
            presentIndex.append([])
            for (id,menu) in enumerate(sectionData){
                if !menu.hidden{
                    presentIndex[section].append(id)
                }
            }
        }
        tableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return presentIndex[section].count
    }
  
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1{
            return "จัดการข้อมูล"
        }else{
            return nil
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMainCell", forIndexPath: indexPath) as! ManuCell

        let section = indexPath.section
        let row = indexPath.row
        let id = presentIndex[section][row]
        let menu = menuSection[section][id]
        
        cell.lb_name.text = menu.title
        cell.img_Manu.image = UIImage(named: menu.image)
        cell.leadMargin.constant = CGFloat(25 * menu.level)
        cell.img_Manu.clipsToBounds = true
        
        return cell
    }

    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            
             dra.append(tabbarController(drawerController: drawerController))
            
            let section = indexPath.section
            let row = indexPath.row
            let id = presentIndex[section][row]
            let menu = menuSection[section][id]
            
            if section == 1 && id == menuSection[1].count-1{
                alertViewExit()
                drawerController.setDrawerState(.Closed, animated: true)
            }else{
                if menu.hasDetail{
                    drawerController.mainViewController = menu.viewController
                    drawerController.setDrawerState(.Closed, animated: true)
                }else{
                    
                    let selectedLevel = menu.level
                    
                    for (var i = id+1 ; i < menuSection[section].count ; i++){
                        let currentMenu = menuSection[section][i]
                        if currentMenu.level > selectedLevel{
                            menuSection[section][i].hidden = !menuSection[section][i].hidden
                        }else{
                            break
                        }
                    }
                    updatePresentIndex()
                    tableView.reloadData()
                }
            }
            if(section == 0 && id == 0){
                drawerController.mainViewController = UIStoryboard.mainNavigation()
            }else{

            }
            if(section == 0 && id == 1){
                drawerController.mainViewController = UIStoryboard.mainTechniSmok()
            }else{
                
            }
            if(section == 0 && id == 2){
                drawerController.mainViewController = UIStoryboard.mainCalendar()
            }else{
                
            }
            if(section == 0 && id == 4){
                drawerController.mainViewController = UIStoryboard.mainTabViewPages()
            }else{
                
            }
            
        }
        tableView.reloadData()
        loadViewName()


    }
    
    func alertViewExit(){
        var refreshAlert = UIAlertController(title: "ออกจากระบบ", message: "คุณต้องการออกจากระบบใช่หรือไม่", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "ยกเลิก", style: .Default, handler: { (action: UIAlertAction!) in
            println("Cancel")
        }))
        refreshAlert.addAction(UIAlertAction(title: "ตกลง", style: .Default, handler: { (action: UIAlertAction!) in
            self.addJsonView()
            //exit(0)
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func addJsonView(){
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newName = NSEntityDescription.insertNewObjectForEntityForName("MainHome", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue("-1", forKey: "nameMain")
        context.save(nil)
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main-6", bundle: nil)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window!.rootViewController = storyboard.instantiateInitialViewController() as? UIViewController
        appDelegate.window!.makeKeyAndVisible()
    }

}

class ManuCell: UITableViewCell {
    @IBOutlet var leadMargin: NSLayoutConstraint!
    @IBOutlet var lb_name: UILabel!
    @IBOutlet var img_Manu: UIImageView!
}



