//
//  List.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/19/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(List)
class List: NSManagedObject {
   
    
    @NSManaged var nameTx:String
    @NSManaged var ageTx:String
    @NSManaged var genderTx:String
    @NSManaged var diseaseTx:String
}
