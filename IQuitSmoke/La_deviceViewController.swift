//
//  La_deviceViewController.swift
//  ไทยไร้ควัน
//
//  Created by Apinat Jitrat on 11/17/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class La_deviceViewController: UIViewController {

    @IBOutlet var img_view: UIImageView!
    @IBOutlet var lb_text: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lb_text.text = myInstance.textPages[3]
        self.img_view.image = UIImage(named: "img_remove_device")
        
        
        
    }
    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
            
        }
    }

}
