//
//  smoking_day.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/24/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class smoking_day: UIViewController {

    
    @IBOutlet weak var tx_day: UILabel!
    @IBAction func goto_Home(sender: AnyObject) {
        dispatch_async(self.mainQueue){
            self.getData()
        }
    }
    
    @IBOutlet weak var next_Home: UIButton!
    
    
    //ตัวแปรในการรับค่าจาก data Json ในเครื่อง
    
    var _name:String?
    var _age:String?
    var _gender:String?
    var _disease:String?
    
    var _email:String?
    var _profile:String?
    var _idFB:String?
    
    var _smok1:String?
    var _smok2:String?
    var _smok3:String?
    var _smok4:String?
    
    var _start:String?
    var _end:String?
    var _totalDay:String?
    var _inprogram:String?
    
    var rs : Array<NSDictionary>!
    
    var _id:String?
    var responseString:NSString?
    let mainQueue = dispatch_get_main_queue()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        next_Home.layer.cornerRadius = 3.0
        next_Home.clipsToBounds = true
    
        loaddataJson()
        
        
    
    }
    
    func getData(){
        // Add ข้อมูลลงในดาต้าเบส-------------------------------------

        let myURL = NSURL(string: "http://fahsai.bloodd.org/iquitsmoke/newRegister.php")
        let request = NSMutableURLRequest(URL: myURL!)
        request.HTTPMethod = "POST"
        
        
        let postString = "userName=\(_name!)&password=&gender=\(_gender!)&age=\(_age!)&email=\(_email!)&profile=\(_profile!)&referId=\(_idFB!)&registerWith=Facebook&disease=\(_disease!)&howLongSmoke=\(_smok1!)&smokeAmount=\(_smok2!)&smokeAfterWakeup=\(_smok3!)&allFriendSmoke=\(_smok4!)&date=\(_totalDay!)&inProgram=\(_inprogram!)"
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            if error != nil {
                println("error=\(error)")
            }else{
                println("Start")

                self.responseString = NSString(data: data, encoding: NSUTF8StringEncoding)!
                self.addDataProfile()
            }
        }
     
        task.resume()
        println("\(responseString)")
    }
    
    func addDataProfile(){
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var newName = NSEntityDescription.insertNewObjectForEntityForName("Id", inManagedObjectContext: context) as! NSManagedObject
        newName.setValue(responseString, forKey: "id")
        newName.setValue(self._gender, forKey: "gender")
        newName.setValue(self._name, forKey: "name")
        newName.setValue(self._age, forKey: "age")
        
        context.save(nil)
        self.addHome()
    }
    
    func loaddataJson(){
        // โหลดข้อมูลวันที่ต้องการจะเลิก  มาใส่ text ----------------------------------
        
        var appDell:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var contextt:NSManagedObjectContext = appDell.managedObjectContext!
        
        let fetchh = NSFetchRequest(entityName: "Calendar")
        fetchh.returnsObjectsAsFaults = false
        var resultss:NSArray = contextt.executeFetchRequest(fetchh, error: nil)!
        
        if(resultss.count > 0){
            
            var res = resultss[resultss.count - 1] as! NSManagedObject
            
            //text ต้องข้อมูล ---------------------------------------------------
            
            tx_day.text = res.valueForKey("id_Day") as? String
            
        }else{
            println("error")
            
        }
        
        //โหลดข้อมูลหน้า Person_information ------------------------------------------
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        let fetch = NSFetchRequest(entityName: "List")
        fetch.returnsObjectsAsFaults = false
        var results:NSArray = context.executeFetchRequest(fetch, error: nil)!
        
        if(results.count > 0){
            
            var res = results[results.count - 1] as! NSManagedObject
            
            var name = res.valueForKey("nameTx") as? String
            var gender = res.valueForKey("genderTx") as? String
            var age = res.valueForKey("ageTx") as? String
            var disease = res.valueForKey("diseaseTx") as? String
            
            self._name = name
            self._gender = gender
            self._age = age
            self._disease = disease
            
            
        }else{
            println("error")
            
        }
        
        //โหลดข้อมูลหน้า  LoginFBViewController   -------------------------------------------
        
        var appDel1:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context1:NSManagedObjectContext = appDel1.managedObjectContext!
        let fetch1 = NSFetchRequest(entityName: "Facebook_id")
        fetch1.returnsObjectsAsFaults = false
        var results1:NSArray = context1.executeFetchRequest(fetch1, error: nil)!
        
        if(results1.count > 0){
            
            var res = results1[results1.count - 1] as! NSManagedObject
            
            //ดึงข้อมูล facebook
            var email = res.valueForKey("email_Fx") as? String
            var profile = res.valueForKey("profile_Fx") as? String
            var idFacebook = res.valueForKey("id_facebook") as? String
            
            
            self._email = email
            self._profile = profile
            self._idFB = idFacebook
            
            
        }else{
            println("error")
            
        }
        
        
        //โหลดข้อมูลหน้า  Data_smoking_controlle--------------------------------------------------
        
        var appDel2:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context2:NSManagedObjectContext = appDel2.managedObjectContext!
        
        
        let fetch2 = NSFetchRequest(entityName: "Smoking")
        fetch2.returnsObjectsAsFaults = false
        var results2:NSArray = context2.executeFetchRequest(fetch2, error: nil)!
        
        
        if(results2.count > 0){
            
            var res = results2[results2.count - 1] as! NSManagedObject
            
            var smok_1 = res.valueForKey("smok_year") as? String
            var smok_2 = res.valueForKey("smok_roll") as? String
            var smok_3 = res.valueForKey("smok_minute") as? String
            var smok_4 = res.valueForKey("smok_person") as? String
            
            self._smok1 = smok_1
            self._smok2 = smok_2
            self._smok3 = smok_3
            self._smok4 = smok_4
            
            
        }else{
            println("error")
            
        }
        //โหลดข้อมูลหน้า calender_day --------------------------------------------------------------
        
        var appDel3:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context3:NSManagedObjectContext = appDel3.managedObjectContext!
        let fetch3 = NSFetchRequest(entityName: "Calendar")
        fetch3.returnsObjectsAsFaults = false
        var results3:NSArray = context3.executeFetchRequest(fetch3, error: nil)!
        
        if(results3.count > 0){
            
            var res = results3[results3.count - 1] as! NSManagedObject
            
            
            var start = res.valueForKey("start_Day") as? String
            var end = res.valueForKey("end_Day") as? String
            var totalDay = res.valueForKey("total_Day") as? String
            var inprogram = res.valueForKey("inprogram") as? String
            
            self._start = start
            self._end = end
            self._totalDay = totalDay
            self._inprogram = inprogram
            
            
        }else{
            println("error")
            
        }
        
    }
  
    func addHome(){
        
        self.performSegueWithIdentifier("Main1", sender: self)
        
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newName = NSEntityDescription.insertNewObjectForEntityForName("MainHome", inManagedObjectContext: context) as! NSManagedObject
        
        newName.setValue("1", forKey: "nameMain")
        println(newName)
        context.save(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
