//
//  Calendar.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/24/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(Calendar)

class Calendar: NSManagedObject {
    
        @NSManaged var id_Day:String
        @NSManaged var end_Day:String
        @NSManaged var start_Day:String
        @NSManaged var total_Day:String
        @NSManaged var inprogram:String
//        @NSManaged var toDay:NSArray
    
}
