//
//  ShareViewController.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 10/16/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import Social
import Foundation
import CoreData

class ShareViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIAlertViewDelegate, UIActionSheetDelegate,UIScrollViewDelegate {

    @IBOutlet var btn_Shared: UIButton!
    @IBAction func btn_Share(sender: UIButton) {
        sender.backgroundColor = UIColor.clearColor()
        facebookSheet()
    }
    
    @IBAction func btn_TouchDown(sender: UIButton) {
        sender.backgroundColor = UIColor(rgba: "#400277BD")
    }
    
    @IBAction func btn_ActionImage(sender: UIButton) {
        actionImage_Camera()
    }
    @IBOutlet var img_View: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_Shared.layer.cornerRadius = 3.0
        btn_Shared.layer.borderColor =  UIColor(rgba: "#4A6EA9").CGColor
        btn_Shared.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func facebookSheet(){
        
        var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        shareToFacebook.setInitialText("ไทยไร้ควัน: ")
        shareToFacebook.addImage(img_View.image)
        self.presentViewController(shareToFacebook, animated: true, completion: nil)
    }
    
    
    func actionImage_Camera(){
        
        var openPic = UIAlertController(title: "เลือกรูปภาพ", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        let image = UIAlertAction(title: "แกลลอรี่", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            var photoPicker = UIImagePickerController()
            photoPicker.delegate = self
            photoPicker.sourceType = .PhotoLibrary
            self.presentViewController(photoPicker, animated: true, completion: nil)
            
        })
        let camera = UIAlertAction(title: "กล้อง", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
                //load the camera interface
                var picker = UIImagePickerController()
                picker.sourceType = UIImagePickerControllerSourceType.Camera
                picker.delegate = self
                picker.allowsEditing = false
                self.presentViewController(picker, animated: true, completion: nil)
            }else{
                //no camera available
                var alert = UIAlertController(title: "Error", message: "There is no camera available", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction)in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
        })
        let cancelAction = UIAlertAction(title: "ยกเลิก", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            println("Cancelled")
            
        })
        openPic.addAction(camera)
        openPic.addAction(cancelAction)
        openPic.addAction(image)
        
        self.presentViewController(openPic, animated: true, completion: nil)
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        img_View.contentMode = UIViewContentMode.ScaleAspectFill
        img_View.layer.masksToBounds = true
        img_View.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismissViewControllerAnimated(false, completion: nil)
            
    }


    @IBAction func didTapOpenButton(sender: UIBarButtonItem) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }

}
