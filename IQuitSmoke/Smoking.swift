//
//  Smoking.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/21/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit
import CoreData
@objc(Smoking)

class Smoking: NSManagedObject {
   
    
    @NSManaged var smok_minute:String
    @NSManaged var smok_person:String
    @NSManaged var smok_roll:String
    @NSManaged var smok_year:String
    
}
