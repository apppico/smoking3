//
//  Data_smokingViewController.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 8/20/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//
// Unused!
import UIKit
import Foundation
import CoreData


class Data_smokingViewController: UIViewController, UIAlertViewDelegate, UIActionSheetDelegate {

    //ระยะเวลาในการดูดบุหรี่ ----------------------------------------------------------------
    @IBOutlet weak var smokeTx: UITextField!
    @IBAction func smokeBt(sender: AnyObject) {
        smokYears()
    }
    @IBOutlet weak var smokeTx2: UITextField!
    @IBAction func smokeBt2(sender: AnyObject) {
        smokRoll()
    }
    @IBOutlet weak var smokeTx3: UITextField!
    @IBAction func smokeBt3(sender: AnyObject) {
        smokAfterWaking_up()
    }
    @IBOutlet weak var smokeTx4: UITextField!
    @IBAction func smokeBt4(sender: AnyObject) {
        smokCloser()
    }
    @IBAction func connext_smokcalendar(sender: AnyObject) {
        connextSaveData()
    }
    @IBOutlet weak var next_FB: UIButton!
    
    var num : [Int] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        next_FB.layer.cornerRadius = 3.0
        next_FB.clipsToBounds = true
        
        smokeTx.text = "1"
        smokeTx2.text = "1"
        smokeTx3.text = "1"
        smokeTx4.text = "1"
        
        borderTextField()
    }
    
    func smokYears(){
        var actionSheet =  UIActionSheet()
        actionSheet.title = "สูบบุหรี่ มาแล้วกี่ปี"
        actionSheet.delegate = self
        
        for i in 1...30{
            
            num.append(i)
            actionSheet.addButtonWithTitle("\(i)")
            
            if(i == 30){
                actionSheet.addButtonWithTitle("มากกว่า 30")
            }
        }
        
        actionSheet.cancelButtonIndex = actionSheet.addButtonWithTitle("Cancel")
        actionSheet.showInView(UIApplication.sharedApplication().keyWindow)
        
    }
        func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
        {
        if (buttonIndex == 30){
            smokeTx.text = "มากกว่า 30"
        }else{
            while buttonIndex<30{
                switch buttonIndex{
                case buttonIndex:
                    return  smokeTx.text = "\(buttonIndex+1)"
                default:
                    return smokeTx.text = ""
                }
            }
        }
    }
    
    
    func smokRoll(){
      let optionMenu = UIAlertController(title: "สูบบุหรี่", message: "วันละกี่มวน", preferredStyle: .ActionSheet)
        for i in 1...40{
            
            num.append(i)
            let smokeing = UIAlertAction(title: "\(i)", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.smokeTx2.text = "\(i)"
            })
            optionMenu.addAction(smokeing)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func smokAfterWaking_up(){
        let optionMenu = UIAlertController(title: "สูบบุหรี่", message: "หลังตื่นนอน", preferredStyle: .ActionSheet)
        for i in 1...60{
            
            num.append(i)
            let smokeing = UIAlertAction(title: "\(i)", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.smokeTx3.text = "\(i)"
                
            })
            optionMenu.addAction(smokeing)
        }
        let total = UIAlertAction(title: "มากกว่า 60", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.smokeTx3.text = "มากกว่า 60"
        })
        optionMenu.addAction(total)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        // 4
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func smokCloser(){
        
    let optionMenu = UIAlertController(title: "จำนวนผู้ใกล้ชิด", message: "กี่คน", preferredStyle: .ActionSheet)
       for i in 1...40{
            num.append(i)
            let smokeing = UIAlertAction(title: "\(i)", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.smokeTx4.text = "\(i)"
            })
            optionMenu.addAction(smokeing)
        }
        let total = UIAlertAction(title: "มากกว่า 40", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.smokeTx4.text = "มากกว่า 40"
        })
        optionMenu.addAction(total)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            println("Cancelled")
        })
        // 4
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }

    func connextSaveData(){
        
        var smok_1:NSString = smokeTx.text
        var smok_2:NSString = smokeTx2.text
        var smok_3:NSString = smokeTx3.text
        var smok_4:NSString = smokeTx4.text
        
        if ( smok_1.isEqualToString("") || smok_2.isEqualToString("") || smok_3.isEqualToString("") || smok_4.isEqualToString("")){
            
            var alertView:UIAlertView = UIAlertView()
            alertView.title = "กรอกข้อมูลไม่ครบ"
            alertView.message = "กรุณากรอกข้อมูลให้ครบทุกช่อง"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            
        } else {
            
            let apptoFacebook = NSBundle.mainBundle().bundleIdentifier
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(apptoFacebook!)
            self.performSegueWithIdentifier("smok_calendar", sender: self)
            
            //Add data Entities //File Model_data //Set AppDelegate
            
            var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            var context:NSManagedObjectContext = appDel.managedObjectContext!
            
            var newName = NSEntityDescription.insertNewObjectForEntityForName("Smoking", inManagedObjectContext: context) as! NSManagedObject
            newName.setValue("" + smokeTx.text, forKey: "smok_year")
            newName.setValue("" + smokeTx2.text, forKey: "smok_roll")
            newName.setValue("" + smokeTx3.text, forKey: "smok_minute")
            newName.setValue("" + smokeTx4.text, forKey: "smok_person")
            
            context.save(nil)
            println(newName)
        }
    }
    
    
    
     func  borderTextField(){
        
        let border_smokTx = CALayer()
        let width_smokTx = CGFloat(2.0)
        border_smokTx.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_smokTx.frame = CGRect(x: 0, y: smokeTx.frame.size.height - width_smokTx, width:  smokeTx.frame.size.width, height: smokeTx.frame.size.height)
        
        border_smokTx.borderWidth = width_smokTx
        smokeTx.layer.addSublayer(border_smokTx)
        smokeTx.layer.masksToBounds = true
        
        
        let border_smokTx2 = CALayer()
        let width_smokTx2 = CGFloat(2.0)
        border_smokTx2.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_smokTx2.frame = CGRect(x: 0, y: smokeTx2.frame.size.height - width_smokTx2, width:  smokeTx2.frame.size.width, height: smokeTx2.frame.size.height)
        
        border_smokTx2.borderWidth = width_smokTx2
        smokeTx2.layer.addSublayer(border_smokTx2)
        smokeTx2.layer.masksToBounds = true
        
        
        let border_smokTx3 = CALayer()
        let width_smokTx3 = CGFloat(2.0)
        border_smokTx3.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_smokTx3.frame = CGRect(x: 0, y: smokeTx3.frame.size.height - width_smokTx3, width:  smokeTx3.frame.size.width, height: smokeTx3.frame.size.height)
        
        border_smokTx3.borderWidth = width_smokTx3
        smokeTx3.layer.addSublayer(border_smokTx3)
        smokeTx3.layer.masksToBounds = true
        
        
        let border_smokTx4 = CALayer()
        let width_smokTx4 = CGFloat(2.0)
        border_smokTx4.borderColor = UIColor(rgba: "#BDBDBD").CGColor
        border_smokTx4.frame = CGRect(x: 0, y: smokeTx4.frame.size.height - width_smokTx4, width:  smokeTx4.frame.size.width, height: smokeTx4.frame.size.height)
        
        border_smokTx4.borderWidth = width_smokTx4
        smokeTx4.layer.addSublayer(border_smokTx4)
        smokeTx4.layer.masksToBounds = true
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
