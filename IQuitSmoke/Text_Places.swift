//
//  Text_Places.swift
//  IQuitSmoke
//
//  Created by Apinat Jitrat on 9/10/2558 BE.
//  Copyright (c) 2558 Apppi co. All rights reserved.
//

import UIKit

class Text_Places: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var view_backgroud: UIView!
    @IBOutlet weak var foreground: UIScrollView!
    @IBOutlet weak var img_Pic: UIImageView!
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_text: UILabel!
    @IBOutlet weak var lb_calendar: UILabel!
   
    @IBAction func btn_TabberView(sender: UIBarButtonItem) {
        let mycell  = self.storyboard?.instantiateViewControllerWithIdentifier("tabberview") as! TabbarViewPage
        self.navigationController?.pushViewController(mycell, animated: true)
    }
    
    var image: UIImage?
    var name_title:String?
    var name_text:String?
    var name_calendar:String?
    var drawerController:KYDrawerController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        foreground.contentSize.height = 1400
        cuttomview()
        img_Pic.layer.cornerRadius = 1
        img_Pic.clipsToBounds = true
        
        view_backgroud.layer.shadowOpacity = 0.2
        view_backgroud.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_backgroud.layer.shadowRadius = 15.0
        view_backgroud.layer.shadowColor = UIColor.blackColor().CGColor
        
        lb_title.layer.shadowColor = UIColor.blackColor().CGColor
        lb_title.layer.shadowOffset = CGSizeMake(10, 10)
        lb_title.layer.shadowRadius = 15
        lb_title.layer.shadowOpacity = 1
        
    }

    func cuttomview(){
        
        if(place.count > 0){
            var res = place[place.count - 1]
            
            title = res.name_t
            lb_title.text = res.name_t
            img_Pic.image = res.image
            lb_text.text = res.text
            lb_calendar.text = res.calendar
            
        }else{
            println("")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



